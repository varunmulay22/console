# frozen_string_literal: true

source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "3.1.2"

# Bundle edge Rails instead: gem "rails", github: "rails/rails", branch: "main"
gem "rails", "~> 7.0.3"

# Needs to be high enough to make sure the ENV is available for gem configuration
gem "dotenv-rails", groups: %i[development test ci]

# Use pg as the database for Active Record
gem "pg"

# Cross-domain requests
gem "rack-cors", "~> 0.4.0"

# Use the Puma web server [https://github.com/puma/puma]
gem "puma"

# Build JSON APIs with ease [https://github.com/rails/jbuilder]
gem "jbuilder"

# Use Redis adapter to run Action Cable in production
gem "redis"

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem "tzinfo-data", platforms: %i[mingw mswin x64_mingw jruby]

# Reduces boot times through caching; required in config/boot.rb
gem "bootsnap", require: false

group :development, :test, :ci do
  gem "activesupport"
  gem "debug", platforms: %i[mri mingw x64_mingw]
  gem "pry-rails"
  gem "listen"
  gem "rspec-rails"
  gem "rspec-sidekiq", require: false
  gem "factory_bot_rails"
  gem "ffaker"
  gem "simplecov", "~> 0.21.2"
  gem "simplecov-cobertura", "~> 2.1"
  gem "rspec_junit_formatter", "~> 0.5.1"
  gem "rails-controller-testing"
  gem "timecop", "~> 0.9.5"

  # Simple linting
  gem "standardrb"
  gem "mdl", "~> 0.11.0"
end

group :development do
  gem "web-console"
  gem "letter_opener"
  gem "ruby-lsp", require: false
end

gem "active_storage_validations"
gem "aws-sdk-s3", require: false
gem "avo", "~> 2.17"
gem "propshaft" # needed by avo
gem "cancancan"
gem "devise-jwt"
gem "devise"
gem "groupify"
gem "image_processing"
gem "amazing_print"
gem "array_enum"
gem "ruby-enum"
gem "rails-healthcheck"
gem "ruby-vips"
gem "sidekiq"
gem "validators"
gem "rest-client"

gem "google-cloud-storage", "~> 1.42"
gem "ransack"

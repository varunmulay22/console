# Select a solution for sharing API specification

## Context and Problem Statement

We want to be able to collaborate effectively on the API specification. To achieve that we need a quick feedback loop between FE and BE developers. Potentially also users of our platform API if we ever decide to share it publicly.

## Considered Options

- Insomnia
- Postman
- Swagger / Open API
- Custom generator

## Decision Outcome

Postman. It's reasonably priced and gives us the features we're after - API documentation, requests collection, custom environments, request chaining, mock servers, offline access, and more.

It's also an industry standard, so it increases the likelihood of future hires' familiarity with it.

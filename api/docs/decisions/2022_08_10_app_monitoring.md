# Application monitoring

## Context and Problem Statement

We need to know what's going on with our application in terms of load, downtime, response times, bugs and security. We also want to be able to catch usage anomalies (eg. too many videos processed, transactions refused etc).

## Considered Options

1. Datadog
2. NPM
3. Prometheus self-hosted
4. Prometheus managed

## Decision Outcome

Given my familiarity with all of the mentioned platforms, we will go with GCP-managed [Prometheus](https://cloud.google.com/stackdriver/docs/managed-prometheus).

It gives us the advantage of customizability, sensible defaults, ease of use, and integration without the headache of configuring and maintaining it.

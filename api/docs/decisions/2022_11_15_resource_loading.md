# Underscored JSON objects

## Context and Problem Statement

We don't want to mix `camelCase` with `under_score` when we interact with the API. Hence the consensus between FE and BE was to receive and send JSON `under_scored`

## Considered Options

- `camelCase`
- `under_score`
- `iNVERTEDcAMELcASE`

## Decision Outcome

- `under_score`

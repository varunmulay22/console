# Resource access management

## Context and Problem Statement

We want to be give our users ability to create workspaces(AKA groups), projects and allow them to manage invites and permissions accordingly.

## Considered Options

- auth0
- groupify
- pundit
- rolify
- cancancan
- roll our own solution from scratch

## Decision Outcome

Groupify and cancancan. Groupify because it has unique way to manage polymorphic relationships between any entities we want. We will be able to allow the users to access the entities (projects, tasks, learning reasources, assets etc.) and perform actions on them based on the permissions assigned.

Thanks to cancancan we gain an authorisation solution that resembles "the rails way" closely. It adds a sprikle of magic where expected and hence makes fetching authorised entities easy and performant. It also offers test helpers that will make it easy for us to ensure we're showing the right entities to the right users.

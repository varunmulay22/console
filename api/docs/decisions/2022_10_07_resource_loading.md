# Loading resources

## Context and Problem Statement

We want to be able to keep the app performant by avoiding back and forth requests between browser and app

## Considered Options

- writing a collection for each resource type
- caching
- bulk loading
- graphql

## Decision Outcome

Bulk loading. Graphql is not feasible at this stage as that would require rearchitecting
FE and BE, but that would certainly let us avoid the back and forth.

Caching is problematic as you usually need a lot of callbacks to invalidate it at the
right moment for both HTTP and in-memory approach

We've started by utilizing a separate controller for each entity type, but now with groupify and cancancan in place we can make it much easier since all permissions to entities are scoped to either group or a project.
By iniviting a user to your group, you share a workspace and hence all the projects with them.
You can also invite a user to a particular project to be able to give more granular access.
This approach allows us to bulk load all the entities related to a project or multiple projects if we need to.
This approach won't work forever but can take us quite far. It was relatively easy to implement and it solves our needs for now

This approach also corresponds nicely with our seed generation script. Testing will be a breeze as we can just collect a bunch of entity ids and check them against user permissions.

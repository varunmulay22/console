# Limit complexity by reasonable use of microservices

## Context and Problem Statement

How can we limit our infrastructure complexity?

## Considered Options

* No microservices until at least 2024. Ideally - never

## Decision Outcome

Chosen option: `No microservices`. This is where you can find a bit more context about the decision - [Architecture doc](https://www.youtube.com/watch?v=y8OnoxKotPQ)

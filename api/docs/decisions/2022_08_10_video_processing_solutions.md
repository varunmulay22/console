# Select video processing solutions

## Context and Problem Statement

A bulk of our work will be about processing and rendering videos. For us to be able to deliver high-quality products for our users we need to select the platforms that are reliable, performant, easy to intergate, and, feature-rich.

## Considered Options

- GCP transcoder
- AWS elastic transcoder
- Bitmovin
- Cloudinary
- FFMPEG with plugins
- Davinci scripts

## Decision Outcome

GCP. It doesn't close any doors for us, hence we will want to depend on GCP as much as possible for basic use cases. In cases where it's not performant or feature-rich enough, we will evaluate Bitmovin and Cloudinary first and other options later to keep our complexity low.

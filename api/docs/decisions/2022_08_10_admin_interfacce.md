# Admin interface for Synura superusers

## Context and Problem Statement

To lower the need for manual fixes from Synura developers, we want to build an admin section without spending too much time on it. Ideally, something that provides declarative configuration or its DSL

## Considered Options

1. RailsAdmin
2. ActiveAdmin
3. Avo
4. Build our own

## Decision Outcome

Given the tradeoffs above, we have chosen Avo because:

1. It's easy to configure, integrate and maintain
2. Feature-rich
3. Extensible - in case we need some extra functionality
4. Reasonably priced - that gives us an added benefit of commercial support, so that we don't have to depend on open-source maintainer goodwill.

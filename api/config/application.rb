# frozen_string_literal: true

require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Console
  # Application
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0
    Bundler.require(*Rails.groups)
    if ["development", "test", "ci"].include? ENV["RAILS_ENV"]
      Dotenv::Railtie.load
    end

    config.generators do |g|
      g.api_only = true
    end

    config.action_dispatch.rescue_responses["CanCan::AccessDenied"] = :unauthorized
    config.active_storage.service_urls_expire_in = 1.minute
    config.active_storage.urls_expire_in = 1.minute
    config.active_storage.content_types_allowed_inline = %w[image/png image/gif image/jpeg image/tiff image/vnd.adobe.photoshop image/vnd.microsoft.icon application/pdf video/mp4]

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
  end
end

# frozen_string_literal: true

Rails.application.routes.draw do
  require "sidekiq/web"
  mount Sidekiq::Web => "/sidekiq"

  Healthcheck.routes(self)
  root to: "healthcheck/healthchecks#check"

  post "/status_webhook", to: "webhooks#create"

  defaults format: :json do
    devise_for :users, controllers: {sessions: "users/sessions",
                                     registrations: "users/registrations",
                                     passwords: "users/passwords",
                                     confirmations: "users/confirmations"}
    resources :assets
    resources :tasks
    resources :learning_resources
    resources :output_files
    resources :asset_versions
    resources :projects do
      resources :assets do
        resources :asset_versions
      end
    end

    get "current_user", to: "users/current_user#show"
    get "bulk_data", to: "users/current_user#bulk_data"
    put "current_user", to: "users/current_user#edit"
    post "upload", to: "assets#create"
  end

  devise_for :admins, path: "admins", controllers: {
    sessions: "admins/sessions"
  }

  authenticate :admin do
    mount Avo::Engine, at: "/admins/avo"
  end
end

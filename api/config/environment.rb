# frozen_string_literal: true

# Load the Rails application.
require_relative "application"

# Base SMTP configuration
ActionMailer::Base.smtp_settings = {
  address: "smtp.sendgrid.net",
  port: "587",
  authentication: :plain,
  user_name: "apikey",
  password: ENV["SENDGRID_API_KEY"],
  domain: "synura.com",
  enable_starttls_auto: true
}

# Initialize the Rails application.
Rails.application.initialize!

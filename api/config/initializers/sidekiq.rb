Sidekiq.configure_server do |config|
  config.redis = {url: ENV["REDIS_URL_SIDEKIQ"]}
  config.logger.level = :info
end

Sidekiq.configure_client do |config|
  config.redis = {url: ENV["REDIS_URL_SIDEKIQ"]}
  config.logger.level = :info
end

class AddUserToProjects < ActiveRecord::Migration[7.0]
  def change
    add_reference :projects, :user, null: true
    Project.all.each do |p|
      p.update_attribute(:user_id, User.last.id) if p.user_id.blank?
    end
    change_column_null :projects, :user_id, false
  end
end

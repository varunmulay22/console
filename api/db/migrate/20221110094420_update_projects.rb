class UpdateProjects < ActiveRecord::Migration[7.0]
  def up
    remove_column :projects, :state

    execute <<-SQL
      DROP TYPE IF EXISTS project_state;
    SQL

    add_column :projects, :state, :integer, default: ProjectPhaseEnum.draft, null: false

    Project.all.each do |project|
      project.update_attribute :state, ProjectPhaseEnum.values.sample
    end
  end
end

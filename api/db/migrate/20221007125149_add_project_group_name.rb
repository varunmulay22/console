class AddProjectGroupName < ActiveRecord::Migration[7.0]
  def change
    add_column :projects, :group_name, :string
  end
end

class RenameProjectStateToProjectPhase < ActiveRecord::Migration[7.0]
  def change
    rename_column :projects, :state, :phase
  end
end

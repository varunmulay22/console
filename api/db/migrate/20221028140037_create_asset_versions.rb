class CreateAssetVersions < ActiveRecord::Migration[7.0]
  def change
    create_table :asset_versions do |t|
      t.integer :performed_operations, array: true, null: false, default: []
      t.text :blob_url
      t.belongs_to :asset, null: false, foreign_key: true
      t.belongs_to :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end

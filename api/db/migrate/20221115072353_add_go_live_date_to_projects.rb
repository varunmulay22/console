class AddGoLiveDateToProjects < ActiveRecord::Migration[7.0]
  def change
    add_column :projects, :go_live_at, :datetime, null: true
  end
end

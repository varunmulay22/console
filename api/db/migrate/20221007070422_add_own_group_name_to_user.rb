class AddOwnGroupNameToUser < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :own_group_name, :string
  end
end

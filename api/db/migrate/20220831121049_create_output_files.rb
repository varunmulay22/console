class CreateOutputFiles < ActiveRecord::Migration[7.0]
  def change
    create_enum :output_file_state, ["draft", "processing", "failed", "ready"]

    create_table :output_files do |t|
      t.string :name
      t.decimal :size
      t.references :project, null: false, foreign_key: true
      t.enum :state, enum_type: "output_file_state", default: "draft", null: false

      t.timestamps
    end
  end
end

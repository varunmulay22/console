class CreateWebhooks < ActiveRecord::Migration[7.0]
  def change
    create_table "webhooks", force: :cascade do |t|
      t.string "url"
      t.integer "state", default: 0, null: false
      t.integer "creator_id", null: false
      t.integer "environment_id", null: false
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.string "secret", limit: 50, null: false
      t.text "description"
      t.index ["environment_id"], name: "index_webhooks_on_environment_id"
    end
  end
end

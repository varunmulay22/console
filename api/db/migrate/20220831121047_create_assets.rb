class CreateAssets < ActiveRecord::Migration[7.0]
  def change
    create_table :assets do |t|
      t.string :name
      t.text :description
      t.references :group, null: true, foreign_key: true

      t.timestamps
    end
  end
end

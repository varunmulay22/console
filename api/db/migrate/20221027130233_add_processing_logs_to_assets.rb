class AddProcessingLogsToAssets < ActiveRecord::Migration[7.0]
  def change
    add_column :assets, :processing_logs, :jsonb, default: []
  end
end

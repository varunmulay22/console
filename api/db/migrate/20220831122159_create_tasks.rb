class CreateTasks < ActiveRecord::Migration[7.0]
  def change
    create_enum :task_state, ["draft", "in_progress", "cancelled", "done"]

    create_table :tasks do |t|
      t.string :description
      t.references :project, null: false, foreign_key: true
      t.enum :state, enum_type: "task_state", default: "draft", null: false

      t.timestamps
    end
  end
end

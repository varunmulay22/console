class CreateProjects < ActiveRecord::Migration[7.0]
  def change
    create_enum :project_state, ["draft", "published", "archived"]

    create_table :projects do |t|
      t.string :name
      t.text :description
      t.references :group, null: true, foreign_key: true
      t.enum :state, enum_type: "project_state", default: "draft", null: false

      t.timestamps
    end
  end
end

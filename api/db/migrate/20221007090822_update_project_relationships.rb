class UpdateProjectRelationships < ActiveRecord::Migration[7.0]
  def change
    add_column :assets, :group_name, :string
    add_column :learning_resources, :group_name, :string
  end
end

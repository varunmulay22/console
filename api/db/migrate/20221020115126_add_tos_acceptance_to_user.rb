class AddTosAcceptanceToUser < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :tos_acceptance, :boolean, default: false

    User.all.each do |u|
      u.update_attribute :tos_acceptance, true
    end
  end
end

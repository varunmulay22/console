2.times do |i|
  user = FactoryBot.create(:user,
    full_name: FFaker::Name.name,
    email: FFaker::Internet.email,
    password: "Synura1234!!!",
    password_confirmation: "Synura1234!!!")

  project = user.projects.first

  3.times do |i|
    project.tasks.create(description: "Play #{FFaker::Game.title}")
  end

  3.times do |i|
    LearningResource.create(
      url: FFaker::Internet.http_url,
      title: FFaker::Game.title,
      project_id: project.id,
      group_name: user.own_group_name
    )
  end
end

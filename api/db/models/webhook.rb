require "securerandom"

class Webhook < ApplicationRecord
  # Regex that webhook url must match.
  URL_REGEX = URI::DEFAULT_PARSER.make_regexp(["http", "https"])

  # Hosts that we don't want to make requests to.
  DENIED_HOSTS = %w[localhost 127.0.0.1].freeze

  has_secure_token :secret

  belongs_to :creator, class_name: "User"
  belongs_to :environment

  has_many :responses, class_name: "WebhookResponse", dependent: :destroy

  delegate :project, to: :environment, allow_nil: true
  delegate :organization, to: :project, allow_nil: true

  enum state: {enabled: 0, deleted: 1, disabled: 2, disabled_by_admin: 3}

  validates :url,
    presence: true,
    format: {with: URL_REGEX, allow_blank: true}
  validates :creator_id, presence: true
  validates :environment_id, presence: true
  validate :url_host

  scope :created, -> { where.not(state: :deleted) }

  def delivered?(uuid)
    responses.where(delivery_id: uuid).exists?
  end

  private

  def url_host
    return if url.blank?
    uri = URI(url)
    if Rails.env.production? && DENIED_HOSTS.include?(uri.host)
      errors.add(:url, "is invalid")
    end
  end
end

require "rails_helper"

RSpec.describe LearningResourcesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/learning_resources").to route_to("learning_resources#index", format: :json)
    end

    it "routes to #show" do
      expect(get: "/learning_resources/1").to route_to("learning_resources#show", id: "1", format: :json)
    end

    it "routes to #create" do
      expect(post: "/learning_resources").to route_to("learning_resources#create", format: :json)
    end

    it "routes to #update via PUT" do
      expect(put: "/learning_resources/1").to route_to("learning_resources#update", id: "1", format: :json)
    end

    it "routes to #update via PATCH" do
      expect(patch: "/learning_resources/1").to route_to("learning_resources#update", id: "1", format: :json)
    end

    it "routes to #destroy" do
      expect(delete: "/learning_resources/1").to route_to("learning_resources#destroy", id: "1", format: :json)
    end
  end
end

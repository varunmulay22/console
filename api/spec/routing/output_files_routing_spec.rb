require "rails_helper"

RSpec.describe OutputFilesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/output_files").to route_to("output_files#index", format: :json)
    end

    it "routes to #show" do
      expect(get: "/output_files/1").to route_to("output_files#show", id: "1", format: :json)
    end

    it "routes to #create" do
      expect(post: "/output_files").to route_to("output_files#create", format: :json)
    end

    it "routes to #update via PUT" do
      expect(put: "/output_files/1").to route_to("output_files#update", id: "1", format: :json)
    end

    it "routes to #update via PATCH" do
      expect(patch: "/output_files/1").to route_to("output_files#update", id: "1", format: :json)
    end

    it "routes to #destroy" do
      expect(delete: "/output_files/1").to route_to("output_files#destroy", id: "1", format: :json)
    end
  end
end

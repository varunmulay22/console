require "rails_helper"

RSpec.describe AssetVersionsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/projects/1/assets/1/asset_versions").to route_to("asset_versions#index", format: :json, project_id: "1", asset_id: "1")
    end

    it "routes to #show" do
      expect(get: "/projects/1/assets/1/asset_versions/1").to route_to("asset_versions#show", id: "1", format: :json, project_id: "1", asset_id: "1")
    end

    it "routes to #create" do
      expect(post: "/projects/1/assets/1/asset_versions").to route_to("asset_versions#create", format: :json, project_id: "1", asset_id: "1")
    end

    it "routes to #update via PUT" do
      expect(put: "/projects/1/assets/1/asset_versions/1").to route_to("asset_versions#update", id: "1", format: :json, project_id: "1", asset_id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/projects/1/assets/1/asset_versions/1").to route_to("asset_versions#update", id: "1", format: :json, project_id: "1", asset_id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/projects/1/assets/1/asset_versions/1").to route_to("asset_versions#destroy", id: "1", format: :json, project_id: "1", asset_id: "1")
    end
  end
end

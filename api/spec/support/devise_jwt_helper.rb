require "devise/jwt/test_helpers"

module DeviseJwtHelper
  def auth_headers(original_headers, user)
    Devise::JWT::TestHelpers.auth_headers(original_headers, user)
  end

  RSpec.configure do |config|
    config.include DeviseJwtHelper, type: :request
  end
end

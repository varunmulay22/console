require "rails_helper"

class FakeController < ApplicationController
  include RequireProjectConcern
end

RSpec.describe FakeController, type: :controller do
  controller do
    def index
      render plain: "index body"
    end
  end

  describe "GET index" do
    before do
      routes.draw { get "index" => "fake#index" }
    end

    describe "with no project" do
      it "throws bad_request unless project_id is present" do
        get :index, params: {}

        expect(response).to have_http_status(:bad_request)
      end
    end

    describe "with project" do
      before do
        project = create(:project)
        controller.instance_variable_set(:@current_user_id, project.user_id)
        get :index, params: {project_id: project.id}
      end

      it "renders if the project_id is present" do
        expect(response.body).to include("index body")
      end

      it "assigns @project if project_id is present" do
        expect(response).to have_http_status(:ok)
        expect(assigns(:project)).to be_a(Project)
      end
    end
  end
end

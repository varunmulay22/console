# frozen_string_literal: true

require "rails_helper"

describe "Login", type: :request do
  let(:user_params) do
    {
      user: attributes_for(:user)
    }
  end

  let(:login_params) do
    {
      email: user_params[:email], password: user_params[:password]
    }
  end

  it "acts accordingly" do
    freeze_time
    post "/users", params: user_params
    initial_access_token = response.headers["Authorization"]
    User.last.confirm

    post "/users/sign_in", params: login_params, headers: {"Authorization" => initial_access_token}
    @access_token = response.headers["Authorization"]

    travel 12.days
    get "/projects", headers: {"Authorization" => @access_token}
    expect(response.status).to eq(200)

    # +3 days since last action; + 15 days since registration
    travel 3.days
    get "/projects", headers: {"Authorization" => @access_token}
    expect(response.status).to eq(401)
  end
end

require "rails_helper"

RSpec.describe "/assets", type: :request do
  context "Authorization" do
    let(:valid_headers) { auth_headers({}, user) }
    let(:project) { create(:project, user: user) }
    let(:asset) { create(:asset, project: create(:project)) }

    context "when asset belongs to project" do
      let(:asset) { create(:asset, project: project) }
      let(:user) { create(:user) }

      it "responds successfully" do
        get project_asset_url(project.id, asset.id), headers: valid_headers
        expect(response).to be_successful
      end
    end

    context "when asset does not belong to project" do
      let(:asset) { create(:asset, project: create(:project)) }
      let(:user) { create(:user) }

      it "returns failed response" do
        get project_asset_url(project.id, asset.id), headers: valid_headers
        expect(response).not_to be_successful
        expect(response.status).to eq(400)
      end
    end

    context "when user does not have access to project" do
      let(:user) { create(:user) }

      it "returns failed response" do
        get project_asset_url(project.id, asset.id), headers: valid_headers
        expect(response).not_to be_successful
        expect(response.status).to eq(400)
      end
    end

    context "when user has access to project" do
      let(:asset) { create(:asset, project: project) }
      let(:user) { create(:user) }

      it "responds successfully" do
        get project_asset_url(project.id, asset.id), headers: valid_headers
        expect(response).to be_successful
      end
    end
  end

  context "skip", skip: true do
    # This should return the minimal set of attributes required to create a valid
    # Asset. As you add validations to Asset, be sure to
    # adjust the attributes here as well.
    let(:valid_attributes) {
      skip("Add a hash of attributes valid for your model")
    }

    let(:invalid_attributes) {
      skip("Add a hash of attributes invalid for your model")
    }

    # This should return the minimal set of values that should be in the headers
    # in order to pass any filters (e.g. authentication) defined in
    # AssetsController, or in your router and rack
    # middleware. Be sure to keep this updated too.
    let(:valid_headers) {
      {}
    }

    describe "GET /index" do
      it "renders a successful response" do
        Asset.create! valid_attributes
        get assets_url, headers: valid_headers, as: :json
        expect(response).to be_successful
      end
    end

    describe "GET /show" do
      it "renders a successful response" do
        asset = Asset.create! valid_attributes
        get asset_url(asset), as: :json
        expect(response).to be_successful
      end
    end

    describe "POST /create" do
      context "with valid parameters" do
        it "creates a new Asset" do
          expect {
            post assets_url,
              params: {asset: valid_attributes}, headers: valid_headers, as: :json
          }.to change(Asset, :count).by(1)
        end

        it "renders a JSON response with the new asset" do
          post assets_url,
            params: {asset: valid_attributes}, headers: valid_headers, as: :json
          expect(response).to have_http_status(:created)
          expect(response.content_type).to match(a_string_including("application/json"))
        end
      end

      context "with invalid parameters" do
        it "does not create a new Asset" do
          expect {
            post assets_url,
              params: {asset: invalid_attributes}, as: :json
          }.to change(Asset, :count).by(0)
        end

        it "renders a JSON response with errors for the new asset" do
          post assets_url,
            params: {asset: invalid_attributes}, headers: valid_headers, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
          expect(response.content_type).to match(a_string_including("application/json"))
        end
      end
    end

    describe "PATCH /update" do
      context "with valid parameters" do
        let(:new_attributes) {
          skip("Add a hash of attributes valid for your model")
        }

        it "updates the requested asset" do
          asset = Asset.create! valid_attributes
          patch asset_url(asset),
            params: {asset: new_attributes}, headers: valid_headers, as: :json
          asset.reload
          skip("Add assertions for updated state")
        end

        it "renders a JSON response with the asset" do
          asset = Asset.create! valid_attributes
          patch asset_url(asset),
            params: {asset: new_attributes}, headers: valid_headers, as: :json
          expect(response).to have_http_status(:ok)
          expect(response.content_type).to match(a_string_including("application/json"))
        end
      end

      context "with invalid parameters" do
        it "renders a JSON response with errors for the asset" do
          asset = Asset.create! valid_attributes
          patch asset_url(asset),
            params: {asset: invalid_attributes}, headers: valid_headers, as: :json
          expect(response).to have_http_status(:unprocessable_entity)
          expect(response.content_type).to match(a_string_including("application/json"))
        end
      end
    end

    describe "DELETE /destroy" do
      it "destroys the requested asset" do
        asset = Asset.create! valid_attributes
        expect {
          delete asset_url(asset), headers: valid_headers, as: :json
        }.to change(Asset, :count).by(-1)
      end
    end
  end
end

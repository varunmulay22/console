require "rails_helper"

RSpec.describe Admin, type: :model do
  let(:synura_user) { build(:admin) }
  let(:not_synura_user) { build(:admin, email: "jason@notsynura.com") }

  describe "authorisation" do
    it "only alows synura admins" do
      expect(synura_user.save).to be(true)
      expect(not_synura_user.save).to be(false)
    end
  end
end

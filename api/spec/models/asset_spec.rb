require "rails_helper"

RSpec.describe Asset, type: :model do
  context "with a valid file" do
    before(:each) do
      @asset = create(:asset)
    end

    it "file is attached" do
      @asset.asset_file.attach(
        io: File.open(Rails.root.join("spec", "fixtures", "sample-avatar.png")),
        filename: "sample-avatar.png",
        content_type: "image/png"
      )
      expect(@asset.asset_file).to be_attached
      # Checking for specific file type as factory gives us a plain text file
      expect(@asset.asset_file.blob.content_type).to eq("image/png")
    end

    it "processing job is enqued" do
      expect {
        create(:asset, :with_image)
      }.to change(ProcessAssetJob.jobs, :size).by(1)
    end

    context "purge" do
      it "removes related attachments" do
        sql = "SELECT COUNT(*) AS attachments_count FROM active_storage_attachments"
        initial_count = ActiveRecord::Base.connection.execute(sql).first["attachments_count"]

        @asset.destroy

        final_count = ActiveRecord::Base.connection.execute(sql).first["attachments_count"]
        expect(initial_count - final_count).to eq(1)
      end

      it "removes related blobs" do
        expect {
          @asset.destroy
        }.to have_enqueued_job(ActiveStorage::PurgeJob)
      end
    end
  end
end

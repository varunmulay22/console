# frozen_string_literal: true

require "rails_helper"

RSpec.describe User, type: :model do
  let(:user) { build(:user) }
  let(:test_user) { create(:user) }

  describe "creation" do
    it "adds a project for the user on creation" do
      expect do
        user.save!
      end.to change { Project.in_any_named_group(user.named_groups).count }.by(1)
    end

    it "adds a group with the right name" do
      user.save!

      group_slug = user.own_group_name

      groups_count = GroupMembership.where(group_name: group_slug).size
      expect(groups_count).not_to be_zero
    end
  end

  describe "groups" do
    it "has ability to invite to its own group" do
      user1 = create(:user)
      user2 = create(:user)

      user2.named_groups.add user1.own_group_name

      expect(user2.named_groups.size).to eq(2)
    end
  end

  it "fails when email is invalid" do
    test_user.email = "invalidemail"
    test_user.save
    expect(test_user.errors[:email]).not_to be(nil)
  end

  it "fails when email is missing" do
    test_user.email = ""
    test_user.save
    expect(test_user.errors[:email]).not_to be(nil)
  end

  it "fails when password is invalid" do
    test_user.password = "12"
    test_user.save
    expect(test_user.errors[:password]).not_to be(nil)
  end

  it "fails when password is missing" do
    test_user.password = ""
    test_user.save
    expect(test_user.errors[:password]).not_to be(nil)
  end

  it "fails when full name is missing" do
    test_user.full_name = ""
    test_user.save
    expect(test_user.errors[:full_name]).not_to be(nil)
  end

  it "requires re-confirmation after changing email" do
    test_user.confirm
    test_user.email = "something@new.com"
    test_user.save!
    expect(test_user.unconfirmed_email).to eq("something@new.com")
  end

  it "requires re-confirmation after changing email" do
    test_user.confirm
    test_user.tos_acceptance = false
    expect(test_user.save).to eq(false)
    test_user.tos_acceptance = true
    expect(test_user.save).to eq(true)
  end
end

require "rails_helper"

RSpec.describe Project, type: :model do
  context "factory" do
    before(:each) do
      @project = build(:project)
    end

    it "can create a project" do
      # It will have to create a user first, which comes with an initial project,
      # hence we expect the count to change by 2
      expect { @project.save }.to change { Project.count }.by(2)
    end
  end

  context "validations" do
    context "phase" do
      it "has draft as a default phase" do
        project = create(:project)
        expect(project.phase).to eq(ProjectPhaseEnum.key(ProjectPhaseEnum.draft).to_s)
      end

      it "validates the phase" do
        ProjectPhaseEnum.keys.each do |phase_value|
          project = build(:project, phase: phase_value)
          expect(project).to be_valid
        end
      end

      it "should raise argument error for wrong phase" do
        expect { build(:project, phase: 111) }.to raise_error(ArgumentError)
      end
    end
  end

  context "destroy when project has tasks" do
    let(:project) { create(:project) }
    let(:task1) { create(:task, project: project) }

    subject(:destroy_project) { project.destroy }

    before do
      task1
    end

    it "destroy task along with project" do
      expect { destroy_project }.to change(Project, :count).by(-1).and change(Task, :count).by(-1)
    end
  end

  context "destroy when project has assets" do
    let(:project) { create(:project) }
    let(:asset) { create(:asset, :with_video, project: project) }

    subject(:destroy_project) { project.destroy }

    before do
      asset
    end

    it "destroy task along with project" do
      expect { destroy_project }.to change(Project, :count).by(-1).and change(Asset, :count).by(-1)
    end
  end
end

FactoryBot.define do
  factory :task do
    description { "MyString" }
    state { "draft" }
    project { nil }
  end
end

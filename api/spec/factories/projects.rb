FactoryBot.define do
  factory :project do
    name { "MyString" }
    description { "MyText" }
    phase { "draft" }
    user
  end

  factory :project_with_tasks, parent: :project do
    after(:create) { |project| create(:task, taskable: project) }
  end
end

FactoryBot.define do
  factory :asset do
    name { "MyString" }
    description { "MyText" }
    group_name { "sample-group" }
    project

    after(:build) do |asset|
      asset.asset_file.attach(io: File.open(Rails.root.join("spec", "fixtures", "sample-txt.txt")),
        filename: "sample-txt.txt",
        content_type: "text/plain")
    end

    trait :with_image do
      after(:build) do |asset|
        asset.asset_file.attach(io: File.open(Rails.root.join("spec", "fixtures", "sample-avatar.png")),
          filename: "sample-avatar.png",
          content_type: "image/png")
      end
    end

    trait :with_video do
      after(:build) do |asset|
        asset.asset_file.attach(io: File.open(Rails.root.join("spec", "fixtures", "sample-vid.mp4")),
          filename: "sample-vid.mp4",
          content_type: "video/mp4")
      end
    end
  end
end

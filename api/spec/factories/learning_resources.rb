FactoryBot.define do
  factory :learning_resource do
    url { "MyString" }
    title { "MyString" }
    project { nil }
  end
end

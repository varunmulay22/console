FactoryBot.define do
  factory :output_file do
    name { "MyString" }
    size { "9.99" }
    project { nil }
    state { "" }
  end
end

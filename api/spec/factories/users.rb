# frozen_string_literal: true

# This will guess the User class
FactoryBot.define do
  sequence :email do |n|
    "email#{n}#{Time.new.strftime("%S.%L")}@example.com"
  end

  factory :user do
    email
    password { "password" }
    password_confirmation { "password" }
    full_name { "John Doe" }
    tos_acceptance { true }
  end
end

FactoryBot.define do
  factory :asset_version do
    performed_operations { 1 }
    blob_url { "MyText" }
    asset { nil }
    user { nil }
  end
end

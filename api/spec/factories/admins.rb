FactoryBot.define do
  sequence :synura_email do |n|
    "email#{n}@synura.com"
  end

  factory :admin do
    email { generate :synura_email }
    password { "password" }
  end
end

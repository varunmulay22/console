require "irb"
require "irb/completion"
require "rubygems"
require "pp"
require "factory_bot"

class Class
  public :include

  # Show only this class class methods
  def class_methods
    (methods - Class.instance_methods - Object.methods).sort
  end

  # Show instance and class methods
  def defined_methods
    methods = {}

    methods[:instance] = new.local_methods
    methods[:class] = class_methods

    methods
  end
end

def reset_passwords
  User.all.each { |u|
    u.password = "Synura1234!!!"
    u.password_confirmation = "Synura1234!!!"
    u.save
  }
end

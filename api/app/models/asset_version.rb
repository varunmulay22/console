class AssetVersion < ApplicationRecord
  include Orderable
  extend ArrayEnum

  belongs_to :asset
  belongs_to :user

  array_enum performed_operations: VideoOperationsEnum.to_h
  has_one_attached :asset_file
end

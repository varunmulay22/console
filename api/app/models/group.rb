class Group < ActiveRecord::Base
  groupify :group, members: [:users, :projects], default_members: :users
end

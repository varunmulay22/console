# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    return unless user.present? # Can't do anything unless logged in
    can :manage, Project, user: user

    can [:read], Project, Project.shares_any_named_group(user) do |project|
      project.shares_any_group?(user)
    end

    can :manage, User, user: user

    can :manage, Asset do |asset|
      user.projects.ids.include?(asset.project_id)
    end

    return unless user.class.is_a? Admin # additional permissions for administrators
    can :manage, Project
    can :manage, User
  end
end

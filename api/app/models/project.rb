class Project < ApplicationRecord
  include Orderable

  enum phase: ProjectPhaseEnum.to_h

  scope :by_go_live_at, -> { order("go_live_at DESC NULLS LAST") }

  has_many :tasks, dependent: :destroy
  has_many :assets, dependent: :destroy
  belongs_to :user
  groupify :named_group_member
  groupify :group
end

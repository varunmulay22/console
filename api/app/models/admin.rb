class Admin < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :validatable

  validate :admin_from_synura

  def admin_from_synura
    if Mail::Address.new(email).domain != "synura.com"
      errors.add(:email, "has to user synura.com domain")
    end
  end
end

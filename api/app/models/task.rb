class Task < ApplicationRecord
  include Orderable

  belongs_to :project
end

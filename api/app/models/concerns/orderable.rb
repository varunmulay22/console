module Orderable
  extend ActiveSupport::Concern

  included do
    scope :by_created, -> { order(created_at: :asc) }
    scope :by_recently_updated, -> { order(updated_at: :desc) }
  end
end

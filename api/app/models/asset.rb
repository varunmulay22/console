class Asset < ApplicationRecord
  include Orderable

  validates :asset_file, presence: true
  has_one_attached :asset_file do |attachable|
    attachable.variant :thumb, resize_to_limit: [500, 500]
  end

  has_many :asset_versions
  belongs_to :project

  groupify :named_group_member

  after_create :process

  def file_path
    if Rails.env.development?
      ActiveStorage::Blob.service.path_for(asset_file.key)
    end
  end

  def process
    ProcessAssetJob.perform_async(id)
  end
end

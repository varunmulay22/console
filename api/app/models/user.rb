# frozen_string_literal: true

class User < ApplicationRecord
  include Devise::JWT::RevocationStrategies::JTIMatcher
  validates :full_name, presence: true

  validate do |user|
    user.errors.add(:base, "Please accept the terms of service") unless user.tos_acceptance == true
  end

  groupify :named_group_member
  groupify :group_member

  after_create :bootstrap_initial_data

  has_one_attached :avatar

  has_many :projects

  validates :avatar, content_type: ["image/png", "image/jpeg"],
    dimension: {width: {min: 200, max: 2400},
                height: {min: 200, max: 2400}}

  after_create :bootstrap_initial_data

  devise :database_authenticatable,
    :registerable,
    :recoverable,
    :rememberable,
    :validatable,
    :confirmable,
    :timeoutable,
    :trackable,
    :jwt_authenticatable,
    jwt_revocation_strategy: self

  def bootstrap_initial_data
    transaction do
      create_user_group
      create_user_project
      set_own_group_name
    end
  end

  def own_group_name
    @own_group_name ||= email.parameterize
  end

  def create_user_group
    named_groups.add own_group_name, as: "owner"
  end

  def create_user_project
    project = Project.create!(name: "My first project", description: "This is an example project you can use to see how Synura works.", user_id: id)
    project.tasks.create!(description: "My first task")
    project.named_groups.add own_group_name
  end

  def set_own_group_name
    update_attribute :own_group_name, own_group_name
  end

  def own_membership
    GroupMembership.where(group_name: own_group_name, membership_type: "owner").first
  end

  def initial_project
    GroupMembership.where(group_name: own_group_name, member_type: "Project").first
  end

  def group_names
    GroupMembership.distinct.where(member_type: "User", member_id: id).pluck(:group_name)
  end

  def project_ids
    GroupMembership.distinct.where(member_type: "Project", group_name: group_names).pluck(:member_id)
  end

  # def projects
  #   Project.find(project_ids)
  # end

  def jwt_payload
    {"id" => id}
  end
end

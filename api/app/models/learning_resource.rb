class LearningResource < ApplicationRecord
  include Orderable

  groupify :named_group_member
end

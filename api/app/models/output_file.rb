class OutputFile < ApplicationRecord
  include Orderable

  belongs_to :project
end

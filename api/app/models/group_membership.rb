class GroupMembership < ActiveRecord::Base
  self.belongs_to_required_by_default = false
  groupify :group_membership
end

json.extract! asset_version, :id, :performed_operations, :blob_url, :asset_id, :user_id, :created_at, :updated_at
json.url asset_version_url(asset_version, format: :json)

json.extract! project, :id, :name, :description, :group_id, :created_at, :updated_at, :phase, :go_live_at
# json.phase ProjectPhaseEnum.key(project.phase).to_s
json.url project_url(project, format: :json)

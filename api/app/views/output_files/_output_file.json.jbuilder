json.extract! output_file, :id, :name, :size, :project_id, :state, :created_at, :updated_at
json.url output_file_url(output_file, format: :json)

json.extract! asset, :id, :name, :description, :group_id, :created_at, :updated_at, :project_id
json.content_type asset.asset_file.blob.content_type
json.file_name asset.asset_file.blob.filename
json.mb_size asset.asset_file.blob.byte_size.to_f.div(1.megabyte).round(1)
json.metadata asset.asset_file.blob.metadata
json.processing_logs asset.processing_logs if asset.processing_logs

if asset.asset_file.present?
  if asset.asset_file.previewable?
    json.preview_url asset.asset_file.blob.preview(resize_to_limit: [500, 500]).processed.url
  end
  if asset.asset_file.representable?
    json.asset_file_url asset.asset_file.url
  end
end

json.url asset_url(asset, format: :json)

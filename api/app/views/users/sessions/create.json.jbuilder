json.user do |json|
  json.partial! "users/user", user: current_user
  json.confirmed user.confirmed?
  unless user.confirmed?
    json.confirmation_token user.confirmation_token
  end
  json.confirmation_token user.confirmation_token
end

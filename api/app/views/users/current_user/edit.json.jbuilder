json.user do |json|
  json.partial! "users/user", user: User.last
end

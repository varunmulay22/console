json.user do |json|
  json.partial! "users/user", user: current_user
end
json.projects @projects do |project|
  json.name project.name
end
json.tasks @tasks do |task|
  json.description task.description
end
json.learning_resources @learning_resources do |lr|
  json.url lr.url
  json.title lr.title
end
json.output_files do
  json []
end

require "active_support/concern"

module RequireProjectConcern
  extend ActiveSupport::Concern

  included do
    before_action :require_project, only: [:index, :show, :create, :edit, :destroy]
    before_action :authorize_user, only: [:index, :show, :create, :edit, :destroy]
  end

  def require_project
    if params[:project_id]
      @project = Project.find(params[:project_id])
    else
      head :bad_request
    end
  end

  def authorize_user
    head :unauthorized unless @project.user_id == @current_user_id
  end
end

class ProjectsController < ApplicationController
  before_action :set_project, except: [:index, :create]

  # GET /projects
  # GET /projects.json
  def index
    @projects = current_user.projects.by_go_live_at
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.new(project_creation_params)
    @project.named_groups.add(current_user.own_group_name)

    if @project.save
      render :show, status: :created, location: @project
    else
      render json: @project.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    if @project.update(project_params)
      render :show, status: :ok, location: @project
    else
      render json: @project.errors, status: :unprocessable_entity
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_project
    project = Project.find(params[:id])
    if current_user.projects.include? project
      @project = project
    else
      head 401, content_type: "application/json"
    end
  end

  # Only allow a list of trusted parameters through.
  def project_params
    params.require(:project).permit(:name, :description, :group_id, :phase, :id, :go_live_at)
  end

  def project_creation_params
    project_params.reverse_merge(user_id: current_user.id)
  end
end

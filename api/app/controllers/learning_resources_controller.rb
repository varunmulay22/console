class LearningResourcesController < ApplicationController
  include RequireProjectConcern

  before_action :set_learning_resource, only: %i[show update destroy]

  # GET /learning_resources
  # GET /learning_resources.json
  def index
    @learning_resources = LearningResource.all.by_created
  end

  # GET /learning_resources/1
  # GET /learning_resources/1.json
  def show
  end

  # POST /learning_resources
  # POST /learning_resources.json
  def create
    @learning_resource = LearningResource.new(learning_resource_params)

    if @learning_resource.save
      render :show, status: :created, location: @learning_resource
    else
      render json: @learning_resource.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /learning_resources/1
  # PATCH/PUT /learning_resources/1.json
  def update
    if @learning_resource.update(learning_resource_params)
      render :show, status: :ok, location: @learning_resource
    else
      render json: @learning_resource.errors, status: :unprocessable_entity
    end
  end

  # DELETE /learning_resources/1
  # DELETE /learning_resources/1.json
  def destroy
    @learning_resource.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_learning_resource
    @learning_resource = LearningResource.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def learning_resource_params
    params.require(:learning_resource).permit(:url, :title, :project_id)
  end
end

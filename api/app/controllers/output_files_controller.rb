class OutputFilesController < ApplicationController
  include RequireProjectConcern

  before_action :set_output_file, only: %i[show update destroy]

  # GET /output_files
  # GET /output_files.json
  def index
    @output_files = OutputFile.all.by_created
  end

  # GET /output_files/1
  # GET /output_files/1.json
  def show
  end

  # POST /output_files
  # POST /output_files.json
  def create
    @output_file = OutputFile.new(output_file_params)

    if @output_file.save
      render :show, status: :created, location: @output_file
    else
      render json: @output_file.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /output_files/1
  # PATCH/PUT /output_files/1.json
  def update
    if @output_file.update(output_file_params)
      render :show, status: :ok, location: @output_file
    else
      render json: @output_file.errors, status: :unprocessable_entity
    end
  end

  # DELETE /output_files/1
  # DELETE /output_files/1.json
  def destroy
    @output_file.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_output_file
    @output_file = OutputFile.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def output_file_params
    params.require(:output_file).permit(:name, :size, :project_id, :state)
  end
end

# frozen_string_literal: true

class Admins::SessionsController < Devise::SessionsController
  respond_to :html
  skip_before_action :force_json

  def after_sign_in_path_for(admin)
    "/admins/avo"
  end

  def after_sign_out_path_for(admin)
    "/admins/avo/sign_in"
  end
end

class Users::CurrentUserController < ApplicationController
  def show
  end

  def bulk_data
    @groups = current_user.named_groups
    @projects = current_user.projects
    @tasks = Task.where(project_id: @projects.pluck(:id))
    @assets = Asset.where(project_id: @projects.pluck(:id))
    @tasks = Task.where(project_id: @projects.pluck(:id))
    @learning_resources = LearningResource.where(project_id: @projects.pluck(:id))
    @output_files = []
    render :bulk_data
  end

  def edit
    @user = User.last

    if @user.update!(current_user_params)
      render @user.as_json
    else
      render json: @user.errors.to_json
    end
  end

  private

  def current_user_params
    params.require(:user).permit(:avatar)
  end
end

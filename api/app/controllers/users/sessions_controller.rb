class Users::SessionsController < Devise::SessionsController
  respond_to :json, :html

  def new
  end

  def create
    self.resource = warden.set_user(current_user, scope: :user)
    render locals: {user: resource}
  end

  private

  def respond_to_on_destroy
    if current_user
      render json: {
        status: 200,
        message: "logged out successfully"
      }, status: :ok
    else
      render json: {
        status: 401,
        message: "Couldn't find an active session."
      }, status: :unauthorized
    end
  end
end

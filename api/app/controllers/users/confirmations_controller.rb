class Users::ConfirmationsController < Devise::RegistrationsController
  # include RackSessionFix
  skip_before_action :force_json
  respond_to :json, :html

  def show
    if (@user = User.find_by(confirmation_token: params[:confirmation_token]))
      @user.confirm
    end
  end

  def create
    if (@user = User.find_by(email: params[:email]))
      @user.send_confirmation_instructions
    end

    # respond with the same status, regardless of the outcome,
    # so that we don't leak data about email presence
    redirect_to "#{ENV["FRONTEND_URI"]}/", allow_other_host: true
  end

  private

  def after_confirmation_path_for(resource_name, resource)
    sign_in(resource)
    "#{ENV["FRONTEND_URI"]}/"
  end
end

class AssetVersionsController < ApplicationController
  include RequireProjectConcern

  before_action :set_asset_version, only: %i[show update destroy]

  # GET /asset_versions
  # GET /asset_versions.json
  def index
    @asset_versions = AssetVersion.all.by_created
  end

  # GET /asset_versions/1
  # GET /asset_versions/1.json
  def show
  end

  # POST /asset_versions
  # POST /asset_versions.json
  def create
    @asset_version = AssetVersion.new(asset_version_params)

    if @asset_version.save
      render :show, status: :created, location: @asset_version
    else
      render json: @asset_version.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /asset_versions/1
  # PATCH/PUT /asset_versions/1.json
  def update
    if @asset_version.update(asset_version_params)
      render :show, status: :ok, location: @asset_version
    else
      render json: @asset_version.errors, status: :unprocessable_entity
    end
  end

  # DELETE /asset_versions/1
  # DELETE /asset_versions/1.json
  def destroy
    @asset_version.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_asset_version
    @asset_version = AssetVersion.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def asset_version_params
    params.require(:asset_version).permit(:performed_operations, :blob_url, :asset_id, :user_id, :asset_id)
  end
end

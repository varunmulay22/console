# frozen_string_literal: true

# ApplicationController
class ApplicationController < ActionController::Base
  # include JsonApiResponders
  wrap_parameters format: :json
  respond_to :json

  protect_from_forgery with: :null_session
  skip_before_action :verify_authenticity_token

  before_action :underscore_params!
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user
  before_action :force_json

  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      # Render 404 rather than default 302
      format.json { render nothing: true, status: :not_found }
    end
  end

  private

  def underscore_params!
    params.deep_transform_keys!(&:underscore)
  end

  def authenticate_user!(options = {})
    head :unauthorized unless signed_in?
  end

  def current_user
    @current_user ||= super || User.find(@current_user_id)
  end

  def signed_in?
    @current_user_id.present?
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:full_name, :email, :password, :password_confirmation, :tos_acceptance])
    devise_parameter_sanitizer.permit(:account_update, keys: [:full_name, :email, :password, :current_password])
  end

  def authenticate_user
    if request.headers["Authorization"].present?
      authenticate_or_request_with_http_token do |token|
        jwt_payload = JWT.decode(token, Devise::JWT.config.secret).first
        @current_user_id = jwt_payload["id"]
      rescue JWT::ExpiredSignature, JWT::VerificationError, JWT::DecodeError
        head :unauthorized
      end
    end
  end

  def force_json
    request.format = "json"
  end
end

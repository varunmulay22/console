class WebhooksController < ApplicationController
  skip_before_action :authenticate_user

  def create
    render json: params.to_json
  end
end

class AssetsController < ApplicationController
  include RequireProjectConcern
  include ActiveStorage::SetCurrent

  before_action :set_asset, only: %i[show update destroy]
  before_action :authorize_user, only: %i[show update destroy index create]

  # GET /assets
  # GET /assets.json
  def index
    @assets = Asset.where(project_id: params[:project_id]).by_recently_updated
  end

  # GET /assets/1
  # GET /assets/1.json
  def show
  end

  # POST /assets
  # POST /assets.json
  def create
    @asset = Asset.new(asset_params)

    if @asset.save
      render :show, status: :created, location: @asset
    else
      render json: @asset.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /assets/1
  # PATCH/PUT /assets/1.json
  def update
    if @asset.update(asset_params)
      render :show, status: :ok, location: @asset
    else
      render json: @asset.errors, status: :unprocessable_entity
    end
  end

  # DELETE /assets/1
  # DELETE /assets/1.json
  def destroy
    @asset.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_asset
    @asset = Asset.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def asset_params
    params.require(:asset).permit(:name, :description, :group_id, :asset_file, :project_id)
  end

  def require_project
    param = params.dig(:asset, :project_id) || params[:project_id]
    if param
      @project = Project.find(param)
    else
      head :bad_request
    end
  end

  def authorize_user
    head :bad_request if @asset && @project && @project.assets.exclude?(@asset)
  end
end

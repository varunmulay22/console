class VideoOperationsEnum
  include Ruby::Enum

  ## Keep this list sorted alphabetically please
  define :audio_denoise, 1
  define :audio_normalise, 2
  define :transcribe_en, 3
  define :umm_removal, 4
  define :video_denoise, 5
end

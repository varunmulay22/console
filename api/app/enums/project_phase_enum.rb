class ProjectPhaseEnum
  include Ruby::Enum

  define :draft, 1
  define :pre_production, 2
  define :production, 3
  define :post_production, 4
  define :publishing, 5
  define :retrospective, 6
  define :archived, 7
end

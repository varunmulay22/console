class UserResource < Avo::BaseResource
  self.title = :full_name
  self.includes = [:projects]
  self.search_query = -> do
    scope.ransack(id_eq: params[:q],
      full_name_cont: params[:q],
      email_cont: params[:q],
      m: "or").result(distinct: false)
  end

  field :id, as: :id, link_to_resource: true, sortable: true
  field :email, as: :text, link_to_resource: true, sortable: true
  field :full_name, as: :text, link_to_resource: true, sortable: true
  field :created_at, as: :date, link_to_resource: false, sortable: true
  field :last_sign_in_at, as: :date, link_to_resource: false, sortable: true
  field :projects, as: :has_many, searchable: true
end

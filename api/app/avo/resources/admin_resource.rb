class AdminResource < Avo::BaseResource
  self.title = :email
  self.includes = []

  field :id, as: :id
  field :email, as: :text
end

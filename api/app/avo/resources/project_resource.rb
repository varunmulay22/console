class ProjectResource < Avo::BaseResource
  self.title = :name
  self.includes = [:assets]
  self.search_query = -> do
    scope.ransack(id_eq: params[:q],
      name_cont: params[:q],
      description_cont: params[:q],
      m: "or").result(distinct: false)
  end

  field :phase, as: :badge, options: {
    info: "draft",
    success: ["pre_production", "production", "post_production", "publishing", "retrospective"],
    warning: "archived"
  }
  field :id, as: :id, link_to_resource: true
  field :name, as: :text, link_to_resource: true
  field :description, as: :text, link_to_resource: true
  field :assets, as: :has_many, searchable: true
end

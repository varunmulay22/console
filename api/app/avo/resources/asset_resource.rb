class AssetResource < Avo::BaseResource
  self.title = :name
  self.includes = [:asset_versions]
  self.search_query = -> do
    scope.ransack(id_eq: params[:q],
      name_cont: params[:q],
      description_cont: params[:q],
      m: "or").result(distinct: false)
  end

  field :id, as: :text, link_to_resource: true
  field :name, as: :text, link_to_resource: true
  field :description, as: :text, link_to_resource: true
  field :created_at, as: :date, hide_on: [:index]
  field :updated_at, as: :date, hide_on: [:index]
  field :group_name, as: :text, hide_on: [:index]
  field :project_id, as: :text
  field :asset_file, as: :file
  field :asset_versions, as: :has_many, use_resource: AssetVersion

  # field 'Preview URL', as: :text do |asset, _, _|
  #   asset.asset_file.blob.preview(resize_to_limit: [500, 500])
  # end

  field "Metadata", as: :code, language: "javascript", hide_on: [:index] do |asset, _, _|
    if asset.asset_file.blob.video?
      ActiveStorage::Analyzer::VideoAnalyzer.new(asset.asset_file.blob).metadata
    elsif asset.asset_file.blob.image?
      asset.asset_file.blob.metadata
    end
  rescue
    false
  end

  field :size, as: :text do |asset, _, _|
    "#{1.0 + asset.asset_file.blob.byte_size.to_f.div(1.megabyte.to_f).round(3)} MB"
  rescue
    false
  end
end

class AssetVersionResource < Avo::BaseResource
  self.title = :id
  self.includes = []
  # self.search_query = -> do
  #   scope.ransack(id_eq: params[:q],
  #     name_cont: params[:q],
  #     description_cont: params[:q],
  #     m: "or").result(distinct: false)
  # end

  field :id, as: :text, link_to_resource: true, hide_on: [:new]
  # field :name, as: :text, link_to_resource: true
  # field :description, as: :text, link_to_resource: true
  field :created_at, as: :date, hide_on: [:new]
  field :updated_at, as: :date, hide_on: [:new]
  # field :group_name, as: :text, hide_on: [:index]
  # field :project_id, as: :text
  field :asset, as: :belongs_to

  field :asset_file, as: :file
  # field :performed_operations, as: :select, options: VideoOperationsEnum.to_h
  field :performed_operations, as: :tags, suggestions: -> { VideoOperationsEnum.keys.map(&:to_s).map(&:titleize) }
  # , default: -> { Time.now.hour < 12 ? 'advanced' : 'beginner' }

  # field 'Preview URL', as: :text do |asset, _, _|
  #   asset.asset_file.blob.preview(resize_to_limit: [500, 500])
  # end

  # field "Metadata", as: :code, language: "javascript", hide_on: [:index] do |asset, _, _|
  #   if asset.asset_file.blob.video?
  #     ActiveStorage::Analyzer::VideoAnalyzer.new(asset.asset_file.blob).metadata
  #   elsif asset.asset_file.blob.image?
  #     asset.asset_file.blob.metadata
  #   end
  # rescue
  #   false
  # end

  # field :size, as: :text do |asset, _, _|
  #   "#{1.0 + asset.asset_file.blob.byte_size.to_f.div(1.megabyte.to_f).round(3)} MB"
  # rescue
  #   false
  # end
end

# class AssetVersionResource < Avo::BaseResource
#   self.title = :id
#   self.includes = []
#   # self.search_query = -> do
#   #   scope.ransack(id_eq: params[:q], m: "or").result(distinct: false)
#   # end

#   field :id, as: :id
#   # add fields here
# end

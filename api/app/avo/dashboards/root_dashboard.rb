class RootDashboard < Avo::Dashboards::BaseDashboard
  self.id = "root_dashboard"
  self.name = "Root dashboard"
  self.description = "Here's the latest data"
  self.grid_cols = 1
  card UsersChart
end

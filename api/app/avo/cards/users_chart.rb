class UsersChart < Avo::Dashboards::ChartkickCard
  self.id = "users_assets_chart"
  self.label = "User and asset creation"
  self.chart_type = :line_chart
  self.cols = 2
  self.rows = 3
  self.scale = true
  self.flush = false

  self.chart_options = {curve: false, library: {plugins: {legend: {display: true}}}}

  def query
    start_date = User.first.created_at.to_date
    end_date = Date.today.to_date

    user_data = (start_date..end_date).map do |day|
      [day, User.where("created_at <= ?", day).count]
    end.to_h

    asset_data = (start_date..end_date).map do |day|
      [day, Asset.where("created_at <= ?", day).count]
    end.to_h

    result [
      {name: "Users", data: user_data},
      {name: "Assets", data: asset_data}
    ]
  end
end

class WebhookJob < ApplicationJob
  queue_as :webhooks

  # Call anywhere in the app with WebhookJob.perform_later(environment, SecureRandom.uuid)
  def perform(environment)
    environment.webhooks.enabled.find_each do |webhook|
      SyncWebhook.call(webhook, uuid: uuid)
    end
  end
end

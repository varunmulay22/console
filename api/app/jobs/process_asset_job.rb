class ProcessAssetJob
  include Sidekiq::Worker
  sidekiq_options queue: "asset_processing"

  def perform(asset_id)
    @asset = Asset.find asset_id
    @response = RestClient.post url, payload, content_type: :json
    @asset.processing_logs << log_entry
    @asset.save
  end

  private

  def payload
    {
      project_id: @asset.to_sgid(expires_in: 2.hours, for: "processing"),
      media_url: @asset.asset_file.url
    }.to_json
  end

  def url
    "#{ENV["VIDEO_PROCESSING_ENDPOINT"]}/sy_remove_fillers"
  end

  def log_entry
    {
      time: Time.now.iso8601,
      code: response.code,
      message: response.body
    }
  end
end

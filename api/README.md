# Console API

Console is our web application for collaborating on videos. It is named after
[mixing consoles](https://en.wikipedia.org/wiki/Mixing_console) as used in radio and television production.

## Development environment setup

1. Install [RVM](https://rvm.io/rvm/install) and Ruby/Bundler version listed in [.ruby-version](.ruby-version) (i.e.,
   `rvm install 3.1.2`)
2. Install [vips](https://formulae.brew.sh/formula/vips) (`sudo apt install libvips42`), or if on Mac (`brew install vips`).
3. If on Linux install Postgres headers (`sudo apt install libpq-dev`), or if on Mac (`brew install libpq`).
4. Install and start Redis ([instructions](https://redis.io/docs/getting-started/installation/))
5. Install gem dependencies (`bundle install`)

### Database

You will need to install [PostgreSQL](https://www.postgresql.org/download/) on your machine. For development use, check
the [.env](.env) file and ensure you have a user and password in the database that matches.

Create the database with `bin/rails db:create` and run migrations with `bin/rails db:migrate`. If you ever want to drop
it so you can recreate it, run `bin/rails db:drop`.

### Running the app

To start Console run `bin/rails server`. It will run on `localhost:3000` by default.

### Admin panel

You can access the admin interface [here](http://localhost:3000/admins/avo)

If your migrations executed as expected you should have a local admin account that you can utilize:

```yaml
email: local@synura.com
password: Synura1234!!!
```

If for some reason admin account wasn't created you can just paste this line in your rails console to create one:

```ruby
Admin.create!(email: "localadmin@synura.com", password: "Synura1234!!!")
```

#### Docker compose

If you want to run the app, database, and the frontend all at once, you can run `docker compose up`. It will run the `main` tags of the console and frontend applications by default.

## Key dependencies

1. Framework: [Rails 7](https://github.com/rails/rails/)
2. Authentication: [Devise](https://github.com/heartcombo/devise/wiki)
3. Admin interface: [Avo](https://avohq.io)
4. Vips: [vips](https://formulae.brew.sh/formula/vips)

## File uploads on localhost

You will have to configure it according to your current needs. The easiest way to make it work in development is local file storage:

Look for this line in your `config/environments/development.rb` file
`- config.active_storage.service = :local`

If you want to test it with actual GCP storage you will need to configure the storage like so:
`- config.active_storage.service = :google_local`

You will also need to have a GCP config file in place at `config/gcp-storage-keyfile.json`
You will find the contents of that file in our password manager of choice - look for `GCP for local development` entry

With this setting in place, your files will get uploaded to a GCP storage bucket. It is safe to test with this setting in place as we have a dedicated bucket just for testing.

## Linters

1. [Standard for Ruby](https://github.com/testdouble/standard)
2. [Markdown Lint (MDL)](https://github.com/markdownlint/markdownlint)

Make sure to enable pre-configured hooks wrapping the above linters by running this command:

`git config core.hooksPath .githooks`

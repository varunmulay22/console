# video-processing-backend

## Initial Steps

### 1- Clone repo

```
git clone https://gitlab.com/synura/console.git
cd console/video-processing/media-processing
```

### 2- Set Environment variables in synura.env file

SERVICE_URL: docker exposed host and port

```
export SERVICE_URL="http://localhost:30000"
```

WEBHOOK_URL: The webhook url that final results will be sent to

- This is sample url (works on the same docker service now)
- Set it to any url, POST endpoint the accepts json payload

```
export WEBHOOK_URL="http://localhost:3000/status_webhook"
```

AWS keys will be supplied privately

```
export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=
```

### 3- Build and run Docker image

```
docker compose up
```

## Usage

### Sample request using curl

Filler removal API

POST Request to SERVICE_URL (as set in env file) calling `/sy_remove_fillers`
`/sy_remove_fillers` is async will return immediately
Current state can be queried using `/progress` endpoint

```
curl -X POST http://localhost:30000/sy_remove_fillers \
-H 'Content-Type: application/json' \
-d '{"project_id":"[ANY_ID]", "media_url":"[DIRECT_MEDIA_URL]", "process_list":["anormalize", "adenoise", "remove_um"]}'
```

Progress API
GET Request to SERVICE_URL (as set in env file) calling `/progress/{job_id}` endpoint

```
curl http://localhost:30000/progress/job_id123
```

Example `/progress` response

```
{"log":[list_of_log_lines], "state":"Running/Stopped/Completed"}
```

Swagger doc url

```
http://localhost:30000/docs
```

### Sample response

The sample response is a relativaly large json because it contains full transcription results and FFmpeg results for possible further processing
Response is in `sample_response.json` file

Final modified video file "result_url"

```
"result_url": "http://localhost:30000/media_files/423/423-out-umm3-20sec.webm.mp4"
```

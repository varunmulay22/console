import requests
import os
import ffmpeg_filters as ffilts
import progress as prog
import aws_transcriber as trn #for uploading result to S3

def parse_aws_results(media_data):

    filler_words = [
    "um", "mhm", "uh", "mm"
    ]

    items = media_data["raw_transcript_res"]["results"]["items"]

    trims = []
    for it in items:
        if it["type"] == "punctuation":
            continue
        
        for alt in it["alternatives"]:
            if alt["content"].lower() in filler_words:
                if float(alt["confidence"]) > 0.75:
                    print(it["start_time"], it["end_time"], alt["confidence"], alt["content"])
                    trims.append(it)
    
    return trims, items

def send_status_webhook(ret):

    url = os.getenv('WEBHOOK_URL')
    response = requests.post(url, json=ret)
    if response.status_code != 200:
        return False

    return True

def generate_video(media_data):

    job_id = media_data["job_id"]

    items = {} #AWS results
    trs = [] #Parsed trimed times
    trims = []
    
    """
    - Process AWS in case remove_um or subtitles are requested
    """
    if media_data["raw_transcript_res"] is not None:
        prog.update_progress(job_id, "Parse Transcription results")
        
        trims, items = parse_aws_results(media_data)
        if len(trims) == 0:
            trs = []
            media_data["rmum_trims"] = trs
        else:
            trs = ffilts.ff_trim_times(trims, media_data["duration_sec"])
            media_data["rmum_trims"] = trs
    
    """
    - Create final FFmpeg line
    """
    ffmpeg_line = ffilts.ff_create_line(media_data)
    if ffmpeg_line is None:
        status = "Failed"
        result_url = None
    else:
        ffmpeg_res = ffilts.run_ffmpeg(job_id, ffmpeg_line)
        if not ffmpeg_res:
            result_url = None
            status = "Failed"

            err = "FFmpeg process failed"
            print(err)
            prog.update_progress(job_id, err, False)
            return None
        else:
            status = "Success"
            result_url = f"{os.getenv('SERVICE_URL')}/{media_data['dst']}"
            trn.upload_file(media_data['dst'], public=True)
            aws_result_url = f"https://s3-{os.getenv('S3_REGION_NAME')}.amazonaws.com/{os.getenv('S3_BUCKET_NAME')}/{media_data['dst']}"

    ret = {
        "job_id": job_id,
        "project_id": job_id,
        "parsed_trim_times": trs,
        "ffmpeg_line": ffmpeg_line,
        "occurrence": len(trims),
        "transcript": items,
        "src_v": media_data["src_v"],
        "src_a": media_data["src_a"],
        "dst": media_data["dst"],
        "result_url": result_url,
        "aws_result_url": aws_result_url,
        "duration_sec": media_data["duration_sec"],
        "status": status
    }

    if not send_status_webhook(ret):
        prog.update_progress(job_id, f"Webhook call failed")
        return None

    prog.update_progress(job_id, "Job Completed", completed=True)

    return ret
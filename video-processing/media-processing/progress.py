

progress_obj = {}

def update_progress(job_id, progress_str, success=True, completed=False):

    global progress_obj

    if job_id not in progress_obj:
        progress_obj[job_id] = {}
        progress_obj[job_id]["log"] = []

    p = f"{job_id}: {progress_str}"
    if not success:
        progress_obj[job_id]["state"] = "Stopped"
        return

    progress_obj[job_id]["state"] = "Running"
    if completed:
        progress_obj[job_id]["state"] = "Completed"
    progress_obj[job_id]["log"].append(p)

    return

def get_progress(job_id):

    ret = {}
    if job_id not in progress_obj:
        ret[job_id] = "Job not found"
        return ret

    return progress_obj[job_id]
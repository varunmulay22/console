from ffmpeg_progress import FfmpegProcess
import progress as prog

def ff_trim_times(its, duration):

    """
    Sample input:
    1.54 2.67 0.9428 Um
    3.24 4.52 0.9609 um
    - convert the trim time to FFmpeg trim filter format
    Trim format:
    0-1.54
    1.54-2.67
    2.67-3.24
    3.24-4.52
    4.52->video duration
    """
    # #Handle case of one filler only
    i = 0
    trs = []

    s = 0
    e = its[i]["start_time"]
    vf = f"v{i}"
    af = f"a{i}"
    vftup = (s,e,vf,s,e,af)
    trs.append(vftup)
    i+=1
    for it in range(0,len(its), 1):
        s = its[it]["end_time"]

        if it+1 == len(its):
            e = float(duration) #video duration
        else:
            e = its[it+1]["start_time"]

        vf = f"v{i}"
        af = f"a{i}"
        vftup = (s,e,vf,s,e,af)
        trs.append(vftup)
        i+=1

    #print(trs)
    return trs

def ff_create_umm_filter_str(in_dic):

    """
    - creates actual FFmpeg line with required filters
    """
    ret = {}

    trims = in_dic["trims"]
    in_v = in_dic["in_v"]
    in_a = in_dic["in_a"]
    out_v = in_dic["out_v"]
    out_a= in_dic["out_a"]

    if len(trims) == 0:
        return ret

    vl = "[{}]trim=start={}:end={},setpts=PTS-STARTPTS,format=yuv420p[{}];[{}]atrim=start={}:end={},asetpts=PTS-STARTPTS[{}];"
    concat_f = "{}concat=n={}:v=1:a=1[{}][{}]"

    trim_l = []
    concat_va = []
    for tr in trims:
        vt = vl.format(in_v, tr[0],tr[1],tr[2], in_a, tr[3],tr[4],tr[5])
        trim_l.append(vt)
        concat_va.append(str("["+tr[2]+"]"))
        concat_va.append(str("["+tr[5]+"]"))
    
    concat_va_join = "".join(concat_va)
    concat_vf = concat_f.format(concat_va_join, str(len(trims)), out_v, out_a)
    trimvf = "".join(trim_l)
    
    ffm_filter = f"{trimvf}{concat_vf}"

    ret = {"filter": ffm_filter, "out_v": out_v, "out_a": out_a}
    return ret

def ff_create_adenoise_filter_str(in_dic):

    """
    - creates actual FFmpeg line with adenoise filter
    """

    in_a = in_dic["in_a"]
    out_a= in_dic["out_a"]

    #ffm_filter = f"[{in_a}]asendcmd=0.0 afftdn  sn  start,asendcmd=0.4  afftdn  sn  stop,afftdn=nr=7:nf=-40[{out_a}]"
    ffm_filter = f"[{in_a}]afftdn=nr=7:nf=-40[{out_a}]"

    ret = {"filter": ffm_filter, "out_a": out_a}
    return ret

def ff_create_anormalize_filter_str(in_dic):

    """
    - creates actual FFmpeg line with anormalize filter
    """

    in_a = in_dic["in_a"]
    out_a = in_dic["out_a"]

    ffm_filter = f"[{in_a}]dynaudnorm[{out_a}]"

    ret = {"filter": ffm_filter, "out_a": out_a}
    return ret

def ff_create_line(media_data):

    inpf = media_data["src_v"]
    outf = media_data["dst"]
    vcodec = media_data["video_codec"]
    acodec = media_data["audio_codec"]

    #Case of no ffmpeg processing: either process list is empty or has "subtitles" only
    if len(media_data["process_list"]) == 0 or (len(media_data["process_list"]) == 1 and "subtitles" in media_data["process_list"]):
        ffmpeg_line = f"ffmpeg -y -i {inpf} -c copy {outf}"
        
        print("ffmpeg line", ffmpeg_line)
        return ffmpeg_line

    ffl = []
    in_v = "0:v"
    in_a = "0:a"
    out_v = "outv"
    out_a = "outa"

    filters_dic = {
        "remove_um": {
            "filt_func": ff_create_umm_filter_str,
            "args": {
                "trims": media_data["rmum_trims"],
                "in_a": in_a,
                "in_v": in_v,
                "out_a": "atrim_out",
                "out_v": "vtrim_out",
            }
        },
        "adenoise": {
            "filt_func": ff_create_adenoise_filter_str,
            "args": {
                "out_a": "adenoise_out"
            }
        },
        "anormalize": {
            "filt_func": ff_create_anormalize_filter_str,
            "args": {
                "out_a": "anorm_out"
            }
        },
    }

    if "remove_um" in media_data["process_list"]:
        if len(media_data["rmum_trims"]) != 0:
            dic_ret = ff_create_umm_filter_str(filters_dic["remove_um"]["args"])
            if "out_v" in dic_ret:
                in_v = out_v = dic_ret["out_v"]
            if "out_a" in dic_ret:
                in_a = out_a = dic_ret["out_a"]
            ffl.append(dic_ret["filter"])

    for p in media_data["process_list"]:
        if p == "remove_um":
            continue

        if p not in filters_dic:
            prog.update_progress(media_data["job_id"], f"{p} is not supported")
            continue

        filters_dic[p]["args"]["in_v"] = in_v
        filters_dic[p]["args"]["in_a"] = in_a

        dic_ret = filters_dic[p]["filt_func"](filters_dic[p]["args"])
        ffl.append(dic_ret["filter"])
        if "out_v" in dic_ret:
            in_v = out_v = dic_ret["out_v"]
        if "out_a" in dic_ret:
            in_a = out_a = dic_ret["out_a"]

    if len(ffl) == 0:
        return None

    ff_str = ";".join(ffl)
    print("FILTER", ff_str)
    if in_v != "0:v":
        in_v = f"[{in_v}]"
    if in_a != "0:a":
        in_a = f"[{in_a}]"

    ffmpeg_line = f"ffmpeg -y -i {inpf} -filter_complex {ff_str}  -map {in_v} -map {in_a} -c:v {vcodec} -c:a {acodec} {outf}"
    
    print("ffmpeg_line", ffmpeg_line)
    return ffmpeg_line

def handle_ffmpeg_progress_info(percentage, speed, eta, estimated_filesize, progress_handler_args=None):
    job_id = progress_handler_args
    prog.update_progress(job_id, f"FFmpeg process: {int(percentage)}% completed. ETA is {int(eta)} seconds.")
    
def run_ffmpeg(job_id, ffmpeg_line):

    ffmpeg_line_list = ffmpeg_line.split()
    process = FfmpegProcess(ffmpeg_line_list)
    process.run(progress_handler=handle_ffmpeg_progress_info, progress_handler_args=job_id, ffmpeg_output_file=f"media_files/{job_id}/{job_id}_ffmpeg.log")

    return True
import os
import aws_transcriber as trn
import generate_video as gv
from pymediainfo import MediaInfo
import moviepy.editor as mp
import requests
import shutil
from pathlib import Path
import progress as prog

def download_file(project_id, media_url):
    """
    Download file logic stub from url like buckets
    """

    try:
        fn = media_url.split('/')[-1]
        tmp_path = f"/tmp/{fn}"

        with requests.get(media_url, stream=True) as r:
            with open(tmp_path, 'wb') as f:
                shutil.copyfileobj(r.raw, f)

        parent_dir = "media_files"
        path = os.path.join(parent_dir, project_id)
        Path(path).mkdir(parents=True, exist_ok=True)

        dst_path = f"{path}/{fn}"
        shutil.move(tmp_path, dst_path)

    except Exception as e:
        print("Error Downloading file", e)
        return None, None

    return dst_path, path

def get_mediainfo(job_id, fn):
    """
    - Extract metadata needed for processing
    - Currently on duration is used
    """

    try:
        media_data = {}
        media_info = MediaInfo.parse(fn)
        media_data["duration_ms"] = int(media_info.tracks[0].duration)
        media_data["duration_sec"] = float(media_info.tracks[0].duration/1000.0)
    except Exception as e:
        prog.update_progress(job_id, f"Wrong input file format: {e}")
        return None

    return media_data

def extract_audio(fn, job_id):

    proj_dir = job_id
    parent_dir = "media_files"

    try:
        path = os.path.join(parent_dir, proj_dir)
        Path(path).mkdir(parents=True, exist_ok=True)
        media_path = f"{path}/{job_id}.mp3"
        
        mp_clip = mp.VideoFileClip(fn)
        mp_clip.audio.write_audiofile(media_path, verbose=True, logger=None)
    
    except Exception as e:
        print("Error extracting audio", e)
        return None

    return media_path

def get_video_codec():

    vcodec = "h264"
    hwacc = os.getenv('HWACCEL')
    if hwacc is not None and "cuda" in hwacc:
        vcodec = "h264_nvenc"

    return vcodec

def process_video(data):

    job_id = data["project_id"] #f"synura-{int(datetime.now().timestamp())}" #should be some kind of project id
    fn = data["media_url"]

    prog.update_progress(job_id, f"Downlading {fn}")

    fn, project_path = download_file(job_id, fn)
    if fn is None:
        err = "Failed to Download file"
        print(err)
        prog.update_progress(job_id, err, False)
        return None

    prog.update_progress(job_id, "Download success")

    prog.update_progress(job_id, "Extracting Metadata")
    media_data = get_mediainfo(job_id, fn)
    if media_data is None:
        err = "Failed to Extract Metadata"
        print(err)
        prog.update_progress(job_id, err, False)
        return None
    
    prog.update_progress(job_id, "Extracting Audio track")
    media_path = extract_audio(fn, job_id)
    if media_path is None:
        err = "Failed to Extract audio track"
        print(err)
        prog.update_progress(job_id, err, False)
        return None
    
    prog.update_progress(job_id, "Extract Audio success")

    media_data["src_v"] = fn
    media_data["src_a"] = media_path
    media_data["video_codec"] = get_video_codec()
    media_data["audio_codec"] = "aac"
    media_data["dst"] = f"{project_path}/{job_id}-out-{os.path.basename(fn)}.mp4"
    media_data["job_id"] = job_id
    media_data["process_list"] = data["process_list"]
    media_data["raw_transcript_res"] = None

    if "subtitles" in media_data["process_list"] or "remove_um" in media_data["process_list"]:
        prog.update_progress(job_id, "Sending audio for AWS transcription")
        transcribe_res = trn.aws_transcribe(media_data)
        if transcribe_res is None:
            err = "Transcription failed"
            print(err)
            prog.update_progress(job_id, err, False)
            return None
    
        prog.update_progress(job_id, "Transcription success")
        media_data["raw_transcript_res"] = transcribe_res
    else:
        prog.update_progress(job_id, "Skipping transcription")

    res = gv.generate_video(media_data)
    
    #print(res)

    return res

import time
import boto3
import requests
import json
import os

AWS_ACCESS_KEY_ID = os.getenv('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY')
S3_BUCKET_NAME = os.getenv('S3_BUCKET_NAME')
S3_REGION_NAME = os.getenv('S3_REGION_NAME')


def transcribe_file(job_name, file_uri, transcribe_client):
    transcribe_client.start_transcription_job(
        TranscriptionJobName=job_name,
        Media={'MediaFileUri': file_uri},
        MediaFormat='mp3',
        IdentifyLanguage=False,
        LanguageCode='en-US',
        Subtitles = {
        'Formats': [
            'vtt','srt'
        ],
        'OutputStartIndex': 1 
        }
    )

    max_tries = 4000
    while max_tries > 0:
        max_tries -= 1
        job = transcribe_client.get_transcription_job(TranscriptionJobName=job_name)
        job_status = job['TranscriptionJob']['TranscriptionJobStatus']
        if job_status in ['COMPLETED', 'FAILED']:
            print(f"Job {job_name} is {job_status}.")
            if job_status == 'COMPLETED':
                return job['TranscriptionJob']['Transcript']['TranscriptFileUri']
            else:
                print(f"Failed!! for {job_name}. Current status is {job_status}.")
            break
        else:
            time.sleep(5)
            print(f"Waiting for {job_name}. Current status is {job_status}.")

    return None

def download_transcript(TranscriptFileUri):

    #print("TranscriptFileUri", TranscriptFileUri)
    data = requests.get(TranscriptFileUri) # (your url)
    json_aws = data.json()

    return json_aws

def upload_file(object_name, public=False):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """
    
    bucket = S3_BUCKET_NAME
    s3_client = boto3.client('s3', aws_access_key_id=AWS_ACCESS_KEY_ID, aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
    try:
        response_upload = s3_client.upload_file(object_name, bucket, object_name)
        print("Upload response:", response_upload)
        if public:
            response_acl = s3_client.put_object_acl(ACL="public-read", Bucket=bucket, Key=object_name)
            print("ACL response:", response_acl)

    except Exception as e:
        print("AWS Upload Exception", e)
        return False
    return True

def aws_transcribe(media_data):

    job_id = media_data["job_id"]
    
    transcribe_client = boto3.client('transcribe', aws_access_key_id=AWS_ACCESS_KEY_ID, aws_secret_access_key=AWS_SECRET_ACCESS_KEY, region_name=S3_REGION_NAME)
    object_name = media_data["src_a"]
    if not upload_file(object_name):
        print("Error uploading", object_name)
        return None

    file_uri = f's3://{S3_BUCKET_NAME}/{object_name}'
    print("AWS FILEURL", file_uri)
    TranscriptFileUri = transcribe_file(job_id, file_uri, transcribe_client)
    job = None
    jcnt = 0
    while TranscriptFileUri is None:
        job = transcribe_client.get_transcription_job(TranscriptionJobName=job_id)
        TranscriptFileUri = job['TranscriptionJob']['Transcript']['TranscriptFileUri']
        jcnt += 1
        if jcnt > 60:
            print("JOB FAILED", job_id)
            return None

    aws_res_json = download_transcript(TranscriptFileUri)
    transcribe_client.delete_transcription_job(TranscriptionJobName=job_id)
    aws_res_json["TranscriptFileUri"] = TranscriptFileUri

    #For debugging
    with open(f"media_files/{job_id}/{job_id}.json", "w") as f:
        json.dump(aws_res_json, f)

    return aws_res_json

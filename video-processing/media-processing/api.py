from fastapi import BackgroundTasks, FastAPI
from fastapi.staticfiles import StaticFiles
import process_video as pv
from pydantic import BaseModel
from fastapi import Request
import progress as prog
import os
from pathlib import Path

app = FastAPI()

path = os.path.join("media_files")
Path(path).mkdir(parents=True, exist_ok=True)
app.mount("/media_files", StaticFiles(directory="media_files"), name="media_files")

class VideoPayload(BaseModel):
    project_id: str
    media_url: str
    process_list: list

#Deprecating /sy_remove_fillers
@app.post("/sy_remove_fillers")
async def sy_remove_fillers(vpayload: VideoPayload, background_tasks: BackgroundTasks):

    req_body = await process_video_endpoint(vpayload, background_tasks)
    req_body["deprecated"] = "/sy_remove_fillers endpoint is deprecated, use /process_video instead"
    return req_body

@app.post("/process_video")
async def process_video_endpoint(vpayload: VideoPayload, background_tasks: BackgroundTasks):

    req_body = {
        "project_id": vpayload.project_id,
        "media_url": vpayload.media_url,
        "process_list": vpayload.process_list,
    }

    background_tasks.add_task(pv.process_video, req_body)
    req_body["status"] = f"{vpayload.project_id} Submitted"

    return req_body

@app.get("/progress/{job_id}")
async def get_progress(job_id):

    job_state = prog.get_progress(job_id)

    return job_state
    
#For debugging
@app.post("/status_webhook")
async def status_webhook(request: Request):
    
    status = await request.body()
    print("Status", status)

    return {}
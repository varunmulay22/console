# High level architecture

## Context and Problem Statement

What is a good enough architecture for us to integrate services we will use?

## Considered Options

1. Docker
2. VM
3. Binaries
4. Direct integration of rails and GCP / AWS encoding APIs
5. Message brokers
6. Own APIs with webhooks

## Decision Outcome

Chosen option: "Docker" & "Own APIs with webhooks" for now. We will use docker to wrap our AWS / GCP calls, as we're not working on any advanced locall processing. It is very likely to be expanded in the future when we will want to take advantage of CUDA SDKs and own deployments of FLOSS tools. It is also likely we will depend less on cloud encoding as running our own FFMPEG backed encoding/decoding services will be more performant and cheaper.

You can see initial design below:

![Draft architecture](./assets/draft-architecture.png "Draft architecture")

# This is the monorepo for Synura

## General

To run all the required services for local development you can use [Foreman](https://github.com/ddollar/foreman)

You'll need to install it first:

```ruby
gem install foreman
```

And then from the repo's root directory run this command:

```bash
foreman start
```

If you see output similar to this...

![foreman output](doc/foreman-output.png "Foreman output")

It means your setup is correct. You'll see all the service logs aggregated in a single terminal window.

## Rails (API)

Rails app will run on port `3000` and frontend will run on port `9000`If you want to see the ruby debugger you can attach it like so:

```bash
rdbg --attach 3002
```

Alternatively you can install `rdgb` for VScode and run it from there

## Frontend

add instructions here...

## Video processing

In order to copy the results of media processing you can use the following command:

```bash
docker cp media-processing-synura-filler-remove-1:/app/media_files ./
```

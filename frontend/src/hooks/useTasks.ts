import { useMemo } from "react";
import { createTask, deleteTask as deleter, updateTask as updater } from "../api/task";
import { Task, NewTask } from "../model/task";
import { useAxios } from "./useAxios";

export const useTasks = (
  projectId: string
): {
  tasks: Task[];
  addTask: (data: NewTask) => void;
  updateTask: (data: Partial<Task>) => void;
  deleteTask: (data: Partial<Task>) => void;
} => {
  const { response, sendData: refetch } = useAxios({
    method: "GET",
    url: "/tasks",
    params: {
      projectId,
    },
  });

  const addTask = (data: NewTask) => {
    createTask(data).then(refetch);
  };

  const updateTask = (data: Partial<Task>) => {
    updater(data).then(refetch);
  };

  const deleteTask = (data: Partial<Task>) => {
    deleter(data).then(refetch);
  };

  const memo = useMemo(
    () => ({
      tasks: response?.data || [],
      addTask,
      updateTask,
      deleteTask,
    }),
    [response?.data, addTask, deleteTask, updateTask]
  );

  return memo;
};

import React, { useEffect, useState } from "react";
import { request } from "../api/request";
import { Text } from "../components/typography/text";

const ERROR_MESSAGE: Record<number, React.ReactNode> = {
  400: (
    <Text>
      This is an invalid request. If you think this should be working, please{" "}
      <a href="https://www.synura.com/handbook/general/earlyaccess/#how-to-get-help-or-give-feedback">contact us</a> to
      let us know.
    </Text>
  ),
  401: (
    <Text>
      You don't seem to have access to this resource. If you think this should be working, please{" "}
      <a href="https://www.synura.com/handbook/general/earlyaccess/#how-to-get-help-or-give-feedback">contact us</a> to
      let us know.
    </Text>
  ),
  404: (
    <Text>
      This resource does not seem to exist. If you think this should be working, please{" "}
      <a href="https://www.synura.com/handbook/general/earlyaccess/#how-to-get-help-or-give-feedback">contact us</a> to
      let us know.
    </Text>
  ),
  500: (
    <Text>
      Sorry, something went wrong. Please try again later or{" "}
      <a href="https://www.synura.com/handbook/general/earlyaccess/#how-to-get-help-or-give-feedback">contact us</a> to
      let us know if it keeps happening.
    </Text>
  ),
  100: (
    <Text>
      The service is unreachable. Please check your internet connection or try again later. You can{" "}
      <a href="https://www.synura.com/handbook/general/earlyaccess/#how-to-get-help-or-give-feedback">contact us</a> to
      let us know if this continues.
    </Text>
  ),
};

export const useRequestInterceptors = () => {
  const [currentError, setCurrentError] = useState<number | null>(null);
  const errorInterceptor = request.interceptors.response.use(
    (result) => {
      if (result.status < 300) {
        setCurrentError(null);
      }
      return result;
    },
    (error) => {
      if (error?.response?.status) {
        switch (error.response.status) {
          case 400:
            setCurrentError(400);
            throw error;
          case 401:
            setCurrentError(401);
            throw error;
          case 404:
            setCurrentError(404);
            throw error;
          case 500:
            setCurrentError(500);
            throw error;
          default:
            throw error;
        }
      } else {
        setCurrentError(100);
        throw error;
      }
    }
  );

  useEffect(() => {
    return () => {
      request.interceptors.response.eject(errorInterceptor);
    };
  });

  return {
    error: currentError ? ERROR_MESSAGE[currentError] : null,
  };
};

import { useState, useMemo } from "react";
import { useSpring } from "@react-spring/web";

export const useErrorAnimation = () => {
  const [state, toggle] = useState(false);

  const { x } = useSpring({
    from: { x: 0 },
    x: state ? 1 : 0,
    config: { duration: 1000 },
  });

  const memo = useMemo(
    () => ({
      state,
      animate: toggle,
      x,
    }),
    [state, toggle, x]
  );

  return memo;
};

import { useState, useEffect } from "react";

export enum PromiseState {
  pending = "pending",
  resolved = "resolved",
  rejected = "rejected",
}

export type UsePromise<T> = [PromiseState, T | undefined, any];

export function usePromise<T>(promise: () => Promise<T>): UsePromise<T> {
  const [state, setState] = useState<PromiseState>(PromiseState.pending);
  const [error, setError] = useState();
  const [result, setResult] = useState<T>();

  useEffect(() => {
    let abort = false;

    (async () => {
      setState(PromiseState.pending);
      setError(undefined);
      setResult(undefined);
      promise()
        .then((value) => {
          if (abort) return;
          setResult(value);
          setState(PromiseState.resolved);
        })
        .catch((error) => {
          if (abort) return;
          setError(error);
          setState(PromiseState.rejected);
        });
    })();

    return () => {
      abort = true;
    };
  }, [promise]);

  return [state, result, error];
}

import React, { createContext, useCallback, useContext, useEffect, useState } from "react";
import { logIn, register, resendConfirmation } from "../api/authentication";

export interface AuthContextProps {
  authenticated: boolean;
  logIn: (email: string, password: string, toggleErrorNotice?: () => void) => void;
  register: (
    fullName: string,
    email: string,
    password: string,
    passwordConfirm: string,
    tos_acceptance: boolean
  ) => void;
  resendConfirmation: (email: string) => void;
  logOut: () => void;
  user?: { name: string; id: number };
  error?: string;
}

export const AuthContext = createContext<AuthContextProps>({
  authenticated: false,
  logIn: (email, password) => logIn(email, password),
  resendConfirmation: (email) => resendConfirmation(email),
  register: (fullName, email, password, passwordConfirm) => {},
  logOut: () => {},
});

export const AuthenticationProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const [error, setError] = useState<string | undefined>(undefined);
  const [authenticated, setAuthenticated] = useState<boolean>(false);
  const [user, setUser] = useState<any>();

  const logInCallback = useCallback(async (email: string, password: string, toggleErrorNotice?: () => void) => {
    const response = await logIn(email, password);

    if (response.error) {
      setError(response.error);
      setAuthenticated(false);
      toggleErrorNotice?.();
    } else {
      let { authorization, ...user } = response;
      setAuthenticated(true);
      setUser(user);
      localStorage.setItem("synura-bearer-token", authorization);
      // authenticatedRequest.defaults.headers.common.Authorization = authorization;
    }
  }, []);

  const registerCallback = useCallback(
    async (fullName: string, email: string, password: string, passwordConfirm: string, tos_acceptance: boolean) => {
      const response = await register(fullName, email, password, passwordConfirm, tos_acceptance);

      if (response.error) {
        setError(response.error);
        setAuthenticated(false);
      } else {
        let { authorization, ...user } = response;
        setAuthenticated(true);
        setUser(user);

        localStorage.setItem("synura-bearer-token", authorization);
        //authenticatedRequest.defaults.headers.common.Authorization = authorization;
      }
    },
    []
  );

  const logOutCallback = useCallback(() => {
    setAuthenticated(false);
    localStorage.removeItem("synura-bearer-token");
  }, []);

  useEffect(() => {
    const token = localStorage.getItem("synura-bearer-token");
    try {
      if (token) {
        const parts = (token as string).split(" ");
        const jwt = parts[1].split(".");
        const user = JSON.parse(window.atob(jwt[1]));
        if (user.exp * 1000 > Date.now()) {
          setAuthenticated(true);
        } else {
          localStorage.removeItem("synura-bearer-token");
          throw new Error("JWT expired, login required");
        }
      }
    } catch (error) {
      console.error(error);
      setAuthenticated(false);
    }
  }, []);

  return (
    <AuthContext.Provider
      value={{
        authenticated,
        error,
        user,
        logIn: logInCallback,
        register: registerCallback,
        resendConfirmation: resendConfirmation,
        logOut: logOutCallback,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export function useAuthentication() {
  return useContext(AuthContext);
}

export type State = "draft" | "in_progress" | "cancelled" | "done";

export type Task = {
  id: number;
  projectId: string | number;
  description: string;
  state: State;
  createdAt: string;
  updatedAt: string;
};

export type NewTask = Omit<Task, "id" | "createdAt" | "updatedAt">;

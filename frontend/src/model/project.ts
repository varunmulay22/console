export enum ProjectPhase {
  Draft = "1",
  Archived = "2",
  PostProduction = "3",
  PreProduction = "4",
  Production = "5",
  Publishing = "6",
  Retrospective = "7",
}

export interface IProjectJSON {
  id: number;
  createdAt?: string;
  updatedAt?: string;
  goLiveAt?: string;
  description?: string;
  groupId?: number;
  name?: string;
  phase?: string;
  url?: string;
}

export interface IProject {
  id: number;
  createdAt?: Date;
  updatedAt?: Date;
  goLiveAt?: string;
  description?: string;
  groupId?: number;
  name?: string;
  phase?: string;
  url?: string;
}

export class Project {
  id: number;
  name: string;
  description?: string;
  createdAt: Date;
  updatedAt: Date;
  goLiveAt: string;
  groupId?: number;
  phase?: string;
  url?: string;

  constructor({
    id = 0,
    name = "",
    description = "",
    createdAt = new Date(),
    updatedAt = new Date(),
    goLiveAt = "",
    groupId,
    phase = ProjectPhase.Draft,
    url,
  }: IProject) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.goLiveAt = goLiveAt;
    this.groupId = groupId;
    this.phase = phase;
    this.url = url;
  }
  static fromJSON(jsonProject: IProjectJSON) {
    return new Project({
      id: Number(jsonProject.id),
      name: jsonProject.name,
      description: jsonProject.description,
      createdAt: new Date(Date.parse(jsonProject.createdAt || "")),
      updatedAt: new Date(Date.parse(jsonProject.updatedAt || "")),
      goLiveAt: jsonProject.goLiveAt,
      groupId: jsonProject.groupId,
      phase: jsonProject.phase,
      url: jsonProject.url,
    });
  }
  toJSON(): IProjectJSON {
    return {
      id: this.id,
      name: this.name,
      description: this.description,
      goLiveAt: this.goLiveAt,
      groupId: this.groupId,
      phase: this.phase,
      // The below fields should not be sent back in requests because they can't be changed
      // createdAt: this.createdAt.toISOString(),
      // updatedAt: this.updatedAt.toISOString(),
      // url: this.url,
    };
  }
}

import * as React from "react";
import styled from "styled-components";
import { useAuthentication } from "../utils/authentication";
import Logo from "./logo";
import { LogoText } from "./typography/logotext";
import { BiMenu, BiHelpCircle, BiLogOut, BiPencil } from "react-icons/bi";
import { useWelcomeNotice, Welcome } from "./welcome";
import { Text } from "./typography/text";
import { useNavigate } from "react-router-dom";
import { useRequestInterceptors } from "../hooks/useRequestInterceptors";
import { useRef } from "react";
import { useOnClickOutside } from "../utils/useOnClickOutside";
import { Menu as OriginalMenu, MenuItem } from "./molecules/Menu";

const Menu = styled(OriginalMenu)`
  position: absolute;
  z-index: 10;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  width: 150px;
`;
const Notice = styled.div`
  margin-top: var(--spacing-sm);
`;
interface HeaderProps {
  title: string;
  children?: React.ReactNode;
}

export const Header: React.FC<HeaderProps> = ({ title, children }) => {
  let navigate = useNavigate();
  const authContext = useAuthentication();
  const { error } = useRequestInterceptors();
  const clickOutsideRef = useRef<HTMLUListElement>(null);
  useOnClickOutside(clickOutsideRef, () => {
    setIsMenuOpen(false);
  });
  const [isMenuOpen, setIsMenuOpen] = React.useState(false);
  const handleOpen = () => {
    setIsMenuOpen(!isMenuOpen);
  };
  const handleLogout = () => {
    authContext.logOut();
    setIsMenuOpen(false);
    navigate("/");
  };
  const handleChangePassword = () => {
    navigate("/change-password");
    setIsMenuOpen(false);
  };
  const { toggleWelcomeNotice, isAcknowledged } = useWelcomeNotice();

  return (
    <div>
      <div
        style={{
          gridArea: "header",
          display: "flex",
          gap: "var(--spacing-md)",
          background:
            window.location.hostname == "console.synura.com" ? "var(--color-nu-10)" : "var(--color-nonprod-header)",
          padding: "var(--spacing-md) var(--spacing-lg)",
          borderBottom: "1px solid var(--color-brand-coolgray)",
          alignItems: "center",
        }}
      >
        <Logo height={17} />
        <LogoText
          style={{
            color: "var(--color-nu-100)",
          }}
        >
          {title}
        </LogoText>
        {children}

        <div style={{ marginLeft: "auto", color: "var(--color-nu-100)", fontSize: "24px", lineHeight: 0 }}>
          <span
            style={{
              color: "var(--color-nu-100)",
              cursor: "pointer",
            }}
            onClick={toggleWelcomeNotice}
          >
            <BiHelpCircle />
          </span>
        </div>

        {authContext.authenticated && (
          <div className="dropdown">
            <div
              onClick={handleOpen}
              style={{
                marginLeft: "1rem",
                color: "var(--color-nu-100)",
                fontSize: "24px",
                lineHeight: 0,
                cursor: "pointer",
              }}
            >
              <BiMenu />
            </div>
            {isMenuOpen ? (
              <Menu ref={clickOutsideRef}>
                <MenuItem onClick={handleLogout}>
                  <BiLogOut />
                  <Text size={10}>Log out</Text>
                </MenuItem>
                <MenuItem onClick={handleChangePassword}>
                  <BiPencil />
                  <Text size={10}>Change password</Text>
                </MenuItem>
              </Menu>
            ) : null}
          </div>
        )}
      </div>
      <Notice>
        <Welcome isAcknowledged={isAcknowledged} toggleWelcomeNotice={toggleWelcomeNotice} />
      </Notice>
      {error && (
        <Notice>
          <div
            style={{
              background: "var(--color-nu-20)",
              maxWidth: "600px",
              margin: "auto",
              display: "flex",
              justifyContent: "center",
              padding: "20px",
              gap: "30px",
              borderRadius: "var(--radius-lg)",
              border: "1px solid var(--color-danger-100)",
            }}
          >
            <div>{error}</div>
          </div>
        </Notice>
      )}
    </div>
  );
};

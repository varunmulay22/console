import { Text } from "./typography/text";
import { useCallback, useMemo, useState } from "react";

export const useWelcomeNotice = () => {
  const [currState, setCurrState] = useState(localStorage.getItem("welcomeAcknowledged"));
  const store = localStorage.getItem("welcomeAcknowledged");

  const toggleWelcomeNotice = useCallback(() => {
    const newState = JSON.parse(store as string) === "true" ? "false" : "true";
    localStorage.setItem("welcomeAcknowledged", JSON.stringify(newState));
    setCurrState(newState);
  }, [store, currState]);

  const memo = useMemo(
    () => ({
      isAcknowledged: JSON.parse(store as string) === "true",
      toggleWelcomeNotice,
    }),
    [store, toggleWelcomeNotice]
  );

  return memo;
};

export const Welcome: React.FC<{ isAcknowledged: boolean; toggleWelcomeNotice: () => void }> = ({
  isAcknowledged,
  toggleWelcomeNotice,
}) => {
  if (isAcknowledged) return null;
  return (
    <div
      style={{
        background: "var(--color-nu-20)",
        maxWidth: "850px",
        margin: "auto",
        display: "flex",
        padding: "20px",
        gap: "30px",
        borderRadius: "var(--radius-lg)",
        borderColor: "var(--color-gradient-30)",
        borderWidth: "1px",
      }}
    >
      <div>
        <Text style={{ textAlign: "center" }}>
          Welcome to{" "}
          <a target="_blank" href="https://www.synura.com">
            Synura
          </a>{" "}
          early access! Visit our{" "}
          <a target="_blank" href="https://www.synura.com/handbook/general/earlyaccess/">
            early access FAQ page
          </a>{" "}
          for more information and to give feedback. You can click the question mark button in the top right to dismiss
          or re-open this message.
        </Text>
      </div>
    </div>
  );
};

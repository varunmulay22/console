import { createStyleObject } from "@capsizecss/core";
import fontMetrics from "@capsizecss/metrics/poppins";
import styled from "styled-components";

export const H3 = styled.h2({
  fontFamily: "Poppins",
  fontWeight: "normal",
  marginBottom: "var(--spacing-md)",
  marginTop: "var(--spacing-md)",
  ...createStyleObject({
    capHeight: 16,
    lineGap: 12,
    fontMetrics,
  }),
});

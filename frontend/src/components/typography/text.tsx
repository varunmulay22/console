import { createStyleObject } from "@capsizecss/core";
import fontMetrics from "@capsizecss/metrics/poppins";
import styled from "styled-components";

interface TextProps {
  /**
   * Cap height in pixels
   */
  size?: number;
  /**
   * Gap between lines in pixels
   */
  lineGap?: number;
  /**
   *
   */
  style?: React.CSSProperties;
  children: React.ReactNode;
}

export const Text = styled.div<TextProps>(({ size = 10, lineGap = 10, style }) => ({
  fontFamily: "Poppins",
  ...createStyleObject({
    capHeight: size,
    lineGap: lineGap,
    fontMetrics,
  }),
  ...style,
}));

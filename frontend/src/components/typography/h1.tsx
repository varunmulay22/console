import { createStyleObject } from "@capsizecss/core";
import fontMetrics from "@capsizecss/metrics/poppins";
import styled from "styled-components";

export const H1 = styled.h1({
  fontFamily: "Poppins",
  fontWeight: "normal",
  marginBottom: "var(--spacing-md)",
  marginTop: "var(--spacing-md)",
  ...createStyleObject({
    capHeight: 20,
    lineGap: 16,
    fontMetrics,
  }),
});

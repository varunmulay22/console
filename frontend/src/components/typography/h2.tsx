import { createStyleObject } from "@capsizecss/core";
import fontMetrics from "@capsizecss/metrics/poppins";
import styled from "styled-components";

export const H2 = styled.h2({
  fontFamily: "Poppins",
  fontWeight: "normal",
  marginBottom: "var(--spacing-md)",
  marginTop: "var(--spacing-md)",
  ...createStyleObject({
    capHeight: 18,
    lineGap: 14,
    fontMetrics,
  }),
});

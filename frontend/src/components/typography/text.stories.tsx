import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Text } from "./text";

export default {
  title: "Typography/Text",
  component: Text,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    size: { control: "select", options: [8, 12, 16, 24, 32] },
    lineGap: { control: "number", step: 4 },
  },
} as ComponentMeta<typeof Text>;

export const Test: ComponentStory<typeof Text> = (args) => <Text {...args}>Hello, World!</Text>;

import React from "react";
import { useParams } from "react-router";
import styled from "styled-components";
import { H3 } from "../typography/h3";
import { useTasks } from "../../hooks/useTasks";
import { TaskForm } from "./TaskForm";
import { Task } from "./Task";
import { Task as TaskItem } from "../../model/task";
import { Text } from "../typography/text";
import { Button } from "../atoms/Button";
import { Menu } from "../molecules/Menu";

type RouteParams = {
  project: string;
};

const AddTask = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-left: var(--spacing-xxl);
  margin-top: var(--spacing-md);
  cursor: pointer;
  color: var(--color-nu-70);
`;

const StyledH3 = styled(H3)`
  margin-bottom: var(--spacing-xl);
`;

export const Tasks: React.FC = () => {
  const { project = "" } = useParams<RouteParams>();
  const [isCreatingTask, setIsCreatingTask] = React.useState(false);

  const { tasks, addTask, updateTask, deleteTask } = useTasks(project);
  const toggleCreatingTask = () => setIsCreatingTask(!isCreatingTask);

  const handleAddTask = (formData: { description: string }) => {
    addTask({ projectId: project, description: formData.description, state: "draft" });
  };

  const handleMarkDone = (task: TaskItem) => {
    const newState = task.state === "draft" ? "done" : "draft";
    updateTask({ id: task.id, projectId: project, description: task.description, state: newState });
  };

  const handleDelete = (task: TaskItem) => {
    deleteTask({ id: task.id, projectId: project });
  };

  if (!project) return null;
  return (
    <div>
      <StyledH3>Project tasks</StyledH3>
      <Menu>
        {tasks.length === 0 && <Text>There are no tasks in this project</Text>}
        {tasks.map((item, idx) => (
          <Task
            key={item.id}
            isEditing={isCreatingTask}
            handleDelete={handleDelete}
            handleMarkDone={handleMarkDone}
            idx={idx}
            task={item}
          />
        ))}
      </Menu>
      <div>
        <AddTask />
        {isCreatingTask ? <TaskForm onSubmit={handleAddTask} /> : null}
        <div style={{ marginTop: "0.5rem", marginBottom: "0.5rem" }}>
          <Button label={isCreatingTask ? "Done" : "Edit tasks"} onClick={toggleCreatingTask} />
        </div>
      </div>
    </div>
  );
};

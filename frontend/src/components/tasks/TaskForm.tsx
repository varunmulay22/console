import * as yup from "yup";
import styled from "styled-components";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Text } from "../../components/typography/text";
import { Input } from "../atoms/Input";
import { Label } from "../atoms/Label";
import { BiPlusCircle } from "react-icons/bi";
import { Row } from "../layout/Row";

const Form = styled.form`
  display: flex;
  align-items: start;
  gap: var(--spacing-xs);
  margin-top: var(--spacing-md);
`;

const Div = styled.div`
  flex: 1;
  input {
    width: 100%;
  }
`;

const ErrorMessage = styled(Text)`
  color: var(--color-brand-primary-100);
  margin-top: var(--spacing-xs);
`;

type Props = {
  description?: string;
  onSubmit: (data: FormInputs) => void;
};

type FormInputs = {
  description: string;
};

const schema = yup
  .object({
    description: yup.string().required("What needs to be done?"),
  })
  .required();

export const TaskForm: React.FC<Props> = ({ description, onSubmit: submit }) => {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<FormInputs>({
    resolver: yupResolver(schema),
    defaultValues: {
      description,
    },
  });
  const onSubmit = (data: FormInputs) => {
    submit(data);
    reset();
  };

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>
      <Div>
        <Label>
          <Row style={{ alignItems: "center" }} gap="var(--spacing-md)">
            <Input data-testid="task-input" autoFocus placeholder="Add a task..." {...register("description")} />
            <BiPlusCircle
              data-testid="task-save"
              size={24}
              role="Button"
              aria-label="Save task"
              onClick={handleSubmit(onSubmit)}
            />
          </Row>

          <ErrorMessage>{errors.description?.message}</ErrorMessage>
        </Label>
      </Div>
    </Form>
  );
};

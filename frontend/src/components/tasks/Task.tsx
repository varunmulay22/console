import React, { useRef, useState } from "react";
import { Task as TaskItem } from "../../model/task";
import styled from "styled-components";
import { Text } from "../typography/text";
import { Label } from "../atoms/Label";
import { BiTrash } from "react-icons/bi";
import { MenuItem } from "../molecules/Menu";
import { useOnClickOutside } from "../../utils/useOnClickOutside";

const Item = styled(MenuItem)`
  display: flex;
  flex-direction: row;
  gap: var(--spacing-sm);
  align-items: center;
  color: var(--color-nu-70);
  cursor: pencil;
`;

const EditTaskInput = styled.input`
  border: 0;
  background: none;
  color: var(--color-nu-100);
  outline-offset: 8px;
  margin-left: -20px;
  padding-left: 20px;
  font-family: "Poppins" sans-serif;
`;

const Spacer = styled.div`
  flex-grow: 1;
`;

const StyledLabel = styled(Label)`
  display: inline-block;
  cursor: pointer;
`;

const Checkbox = styled.input`
  padding-left: var(--spacing-lg);
  background: var(--color-nu-10);
  border: 1px solid var(--color-nu-90);
  border-radius: var(--radius-sm);
  color: var(--color-nu-90);
`;
type Props = {
  idx: number;
  task: TaskItem;
  isEditing: boolean;
  handleMarkDone: (task: TaskItem) => void;
  handleDelete: (task: TaskItem) => void;
};

export const Task: React.FC<Props> = ({ idx, task, isEditing, handleDelete, handleMarkDone }) => {
  const isComplete = task.state === "done";

  const [editingThisTask, setEditingThisTask] = useState(false);
  const [editValue, setEditValue] = useState(task.description);
  const inputRef = useRef<HTMLInputElement>(null);
  const saveEditedTask = () => {
    setEditingThisTask(false);
    handleMarkDone({ ...task, state: isComplete ? "draft" : "done", description: editValue });
  };
  useOnClickOutside(inputRef, saveEditedTask);

  return (
    <Item
      onClick={(evt) => {
        evt.preventDefault();
        if (isEditing) {
          setEditingThisTask(true);
        } else {
          handleMarkDone(task);
        }
      }}
    >
      <Checkbox
        type="checkbox"
        name={`task-item-${idx}`}
        id={`task-item-${idx}`}
        checked={isComplete}
        onClick={(evt) => {
          evt.preventDefault();
          evt.stopPropagation();
          handleMarkDone(task);
        }}
      />
      <StyledLabel htmlFor={`task-item-${idx}`}>
        <Text data-testid="task-rename" style={{ textDecoration: isComplete ? "line-through" : "none" }}>
          {editingThisTask ? (
            <EditTaskInput
              autoFocus
              ref={inputRef}
              value={editValue}
              onChange={(e) => {
                setEditValue(e.target.value);
              }}
              onKeyUp={(event) => {
                if (event.key === "Enter") {
                  saveEditedTask();
                }
              }}
            />
          ) : (
            task.description
          )}
        </Text>
      </StyledLabel>
      <Spacer />
      {isEditing && (
        <>
          <BiTrash
            data-testid="task-delete"
            color="var(--color-gradient-10)"
            role="button"
            aria-label="Delete"
            onClick={() => {
              if (window.confirm("Are you sure you want to delete this task? This cannot be undone.")) {
                handleDelete(task);
              }
            }}
          />
        </>
      )}
    </Item>
  );
};

import * as yup from "yup";
import styled from "styled-components";
import { useNavigate } from "react-router";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Text } from "../components/typography/text";
import { StyledButton } from "../components/atoms/Button";
import { Input } from "../components/atoms/Input";
import { Intent } from "../components/atoms/Intent";
import { Label } from "./atoms/Label";

const Form = styled.form`
  display: flex;
  flex: 1;
  flex-direction: column;
  gap: var(--spacing-md);
  margin-top: var(--spacing-md);
`;

const Div = styled.div`
  flex: 1;
  input {
    width: 100%;
  }
`;

const Actions = styled.div`
  display: flex;
  gap: var(--spacing-md);
  flex: 1;
`;

const ErrorMessage = styled(Text)`
  color: var(--color-brand-primary-100);
  margin-top: var(--spacing-xs);
`;

const Button = styled(StyledButton)`
  display: flex;
  align-items: center;
`;

type Props = {
  defaultValues?: { name: string; description: string; phase: string };
  buttonText?: string;
  onSubmit: (data: FormInputs) => void;
};

type FormInputs = {
  name: string;
  description: string;
};

const schema = yup
  .object({
    name: yup.string().required("This is a required field"),
    description: yup.string(),
  })
  .required();

export const ProjectForm: React.FC<Props> = ({ defaultValues, buttonText, onSubmit: submit }) => {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<FormInputs>({
    resolver: yupResolver(schema),
    defaultValues,
  });
  const navigate = useNavigate();
  const handleGoBack = () => navigate("/");
  const onSubmit = (data: FormInputs) => {
    submit(data);
    reset();
  };

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>
      <Div>
        <Label>
          <Text>Name</Text>
          <Input {...register("name")} />
          <ErrorMessage>{errors.name?.message}</ErrorMessage>
        </Label>
      </Div>
      <Div>
        <Label>
          <Text>Description</Text>
          <Input {...register("description")} />
          <ErrorMessage>{errors.description?.message}</ErrorMessage>
        </Label>
      </Div>
      <Actions>
        <Button intent={Intent.Info} as="button" type="submit">
          <Text>{buttonText}</Text>
        </Button>
        <Button secondary onClick={handleGoBack} intent={Intent.Info} as="button">
          <Text>Cancel</Text>
        </Button>
      </Actions>
    </Form>
  );
};

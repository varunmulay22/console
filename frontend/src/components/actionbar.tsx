interface IActionBarProps {
  actions: React.ReactNode;
}

export const ActionBar: React.FC<IActionBarProps> = ({ actions }) => {
  return <div style={{ display: "flex", gap: "1rem" }}>{actions}</div>;
};

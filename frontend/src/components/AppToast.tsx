import { Toaster, ToastBar, toast } from "react-hot-toast";
import { IoMdClose } from "react-icons/io";
import { Text } from "./typography/text";

export const AppToast: React.FC = () => (
  <Toaster
    position="top-center"
    gutter={8}
    toastOptions={{
      duration: 5000,
      style: {
        background: "var(--color-nu-20)",
        borderRadius: "var(--radius-lg)",
        borderColor: "var(--color-gradient-30)",
        borderWidth: "1px",
        padding: "20px",
        color: "var(--color-nu-100)",
      },
    }}
  >
    {(t) => (
      <ToastBar toast={t}>
        {({ icon, message }) => (
          <>
            {icon}
            <Text>{message}</Text>
            {t.type !== "loading" && (
              <IoMdClose
                data-testid="app-toast"
                style={{
                  fontWeight: "bold",
                  fontSize: "var(--spacing-xxl)",
                  marginLeft: "var(--spacing-md)",
                  color: "var(--color-nu-70)",
                  cursor: "pointer",
                }}
                onClick={() => toast.dismiss(t.id)}
              />
            )}
          </>
        )}
      </ToastBar>
    )}
  </Toaster>
);

import styled from "styled-components";

export const Menu = styled.ul`
  background-color: var(--color-nu-20);
  min-width: 250px;

  list-style-type: none;
  padding: 0;
  border-radius: var(--radius-md);

  right: 0;
`;

export const MenuItem = styled.li`
  width: 100%;
  height: 100%;
  min-height: 42px;
  text-align: left;
  display: flex;
  align-items: center;
  gap: var(--spacing-sm);

  background: none;
  color: inherit;
  border: none;
  padding: var(--spacing-md);
  cursor: pointer;
  border-radius: var(--radius-md);
  border-bottom: 1px solid var(--color-nu-30);

  &:hover {
    background-color: var(--color-nu-10);
  }
`;

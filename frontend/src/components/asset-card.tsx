interface IAssetCardProps {
  [key: string]: any;
  width?: string;
  height?: string;
}

export const AssetCard = ({ width = "192px", height = "108px", ...props }: IAssetCardProps) => {
  if (props?.contentType?.indexOf("image") === 0) {
    return (
      <div
        style={{
          backgroundColor: "var(--color-nu-20)",
          backgroundImage: `url(${props.assetFileUrl})`,
          backgroundSize: "contain",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          width: width,
          height: height,
          borderRadius: "var(--radius-md)",
        }}
      />
    );
  } else if (props?.contentType?.indexOf("application/pdf") === 0) {
    return (
      <div
        style={{
          backgroundColor: "var(--color-nu-20)",
          backgroundImage: `url(${props.previewUrl})`,
          backgroundSize: "contain",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          width: width,
          height: height,
          borderRadius: "var(--radius-md)",
        }}
      />
    );
  } else if (props?.contentType?.indexOf("video") === 0) {
    return (
      <>
        <video
          width={width}
          height={height}
          style={{ background: "var(--color-nu-20)", borderRadius: "var(--radius-md)" }}
          src={props.assetFileUrl}
          controls
        />
      </>
    );
  }
  return null;
};

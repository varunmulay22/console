import React, { Component, ErrorInfo, useContext } from "react";
import styled from "styled-components";
import { useNavigate } from "react-router-dom";
import { Text } from "../components/typography/text";
import LoginBgASvg from "../pages/login/login-bg-a.svg";
import LoginBgBSvg from "../pages/login/login-bg-b.svg";
import { StyledButton } from "./atoms/Button";
import { Intent } from "./atoms/Intent";
import { AuthContext } from "../utils/authentication";

const withNavigation = (Component: React.ComponentClass<any>) => (props: any) => {
  const navigate = useNavigate();
  const { logOut } = useContext(AuthContext);
  return <Component {...props} logOut={logOut} navigate={navigate} />;
};

const ErrorContainer = styled.main`
  background: var(--color-brand-coolgray);
  color: var(--color-nu-90);
  padding: var(--spacing-xxl);
  border-radius: var(--radius-xl);
  max-width: 600px;
  min-width: 600px;
  margin: var(--spacing-xl) auto;
  position: relative;
  z-index: 1;
  display: flex;
  flex-direction: column;
  gap: var(--spacing-lg);
`;

type State = {
  hasError: boolean;
};

type Props = {
  logOut: () => void;
  navigate: (path: string) => void;
  children: React.ReactNode;
};

class ErrorBoundary extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { hasError: false };
  }
  static getDerivedStateFromError() {
    return { hasError: true };
  }
  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    console.log(error, errorInfo);
  }

  redirectToHome = () => {
    this.props.logOut();
    this.props.navigate("/");
    window.location.reload();
  };

  render() {
    if (!this.state.hasError) {
      return this.props.children;
    }
    return (
      <>
        <LoginBgASvg />
        <LoginBgBSvg />

        <ErrorContainer>
          <Text style={{ marginTop: "var(--spacing-xs)" }} size={12}>
            Sorry... Something went wrong. <br />
            Please try again, or{" "}
            <a
              style={{ color: "var(--color-brand-primary-100)" }}
              href="https://www.synura.com/handbook/general/earlyaccess/#how-to-get-help-or-give-feedback"
            >
              let us know
            </a>{" "}
            if the problem persists.
          </Text>
          <div>
            <StyledButton style={{ cursor: "pointer" }} onClick={this.redirectToHome} intent={Intent.Info}>
              <Text>Go Home</Text>
            </StyledButton>
          </div>
        </ErrorContainer>
      </>
    );
  }
}

export default withNavigation(ErrorBoundary);

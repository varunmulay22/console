import { H1 } from "./typography/h1";
import { Text } from "./typography/text";

import blankProjectLogo from "../images/blank_project.svg";
import { Card } from "./atoms/Card";

interface ITitleCardProps {
  title: React.ReactNode;
  subtitle: React.ReactNode;
  description?: React.ReactNode;
  iconUrl?: string;
}

export const TitleCard: React.FC<ITitleCardProps> = ({ title, subtitle, description, iconUrl }) => (
  <Card
    style={{
      display: "flex",
      padding: "20px",
      gap: "20px",
    }}
  >
    <div
      className="hide-xsmall"
      style={{
        width: "100px",
        height: "100px",
        backgroundImage: `url(${iconUrl || blankProjectLogo})`,
        backgroundSize: "95% 90%",
        backgroundRepeat: "no-repeat",
        outlineOffset: "2px",
      }}
    />
    <div>
      <H1 style={{ marginBottom: "var(--spacing-lg)" }}>{title}</H1>
      <Text size={10} style={{ color: "var(--color-nu-60)", marginBottom: "var(--spacing-md)" }}>
        {description}
      </Text>
      <Text
        size={10}
        style={{ color: "var(--color-nu-50)", marginBottom: "var(--spacing-md)", textTransform: "capitalize" }}
      >
        {subtitle}
      </Text>
    </div>
  </Card>
);

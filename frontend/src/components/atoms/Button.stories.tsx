import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Button } from "./Button";
import { Intent } from "./Intent";
import { Size } from "./Size";

export default {
  title: "Foundation/Button",
  component: Button,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {},
} as ComponentMeta<typeof Button>;

export const Defaults: ComponentStory<typeof Button> = (args) => <Button label="Hello, World!" {...args} />;

export const Sizes: React.FC = () => {
  return (
    <>
      <Button size={Size.Small} label="Small" />
      <Button size={Size.Medium} label="Medium" />
      <Button size={Size.Large} label="Large" />
    </>
  );
};

export const Intents: React.FC = () => {
  return (
    <>
      <Button intent={Intent.None} label="None" />
      <Button intent={Intent.Info} label="Info" />
      <Button intent={Intent.Success} label="Success" />
      <Button intent={Intent.Warning} label="Warning" />
      <Button intent={Intent.Danger} label="Danger" />
    </>
  );
};

import styled, { keyframes } from "styled-components";

const pulse = keyframes`
0%, 50% {transform: scale(1);}
15% {transform: scale(0.25);}
`;

function phaseToColor({ phase }: { phase: number }): string {
  const colors = ["#ff4e2d", "#ffc720", "#5fbb5d", "#6087d5", "#6c34e8"];
  return colors[phase % colors.length];
}

const Container = styled.div`
  width: 150px;
  height: 150px;
  display: flex;
  flex-wrap: wrap;
`;

const Item = styled.div<{ phase: number }>`
  width: 50px;
  height: 50px;
  animation: ${pulse} 3s infinite ease-in-out;
  animation-delay: ${({ phase }) => phase / 4}s;
  background: ${phaseToColor};
`;

export const Loader = () => {
  return (
    <Container>
      <Item phase={0} />
      <Item phase={1} />
      <Item phase={2} />

      <Item phase={1} />
      <Item phase={2} />
      <Item phase={3} />

      <Item phase={2} />
      <Item phase={3} />
      <Item phase={4} />
    </Container>
  );
};

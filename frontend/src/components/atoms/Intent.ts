export enum Intent {
  None = "Intent.None",
  Info = "Intent.Info",
  Warning = "Intent.Warning",
  Danger = "Intent.Danger",
  Success = "Intent.Success",
}

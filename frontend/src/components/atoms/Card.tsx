import styled from "styled-components";

export const Card = styled.div`
  background: var(--color-nu-10);
  color: var(--color-nu-90);
  border-radius: var(--radius-lg);
`;

import styled from "styled-components";

interface IAvatarProps {
  url: string;
}

export const Avatar = styled.div<IAvatarProps>`
  width: 40px;
  height: 40px;
  border-radius: 100%;
  background-image: url(${({ url }) => url});
  outline-offset: 2px;
  background-size: cover;
  background-position: center;
`;

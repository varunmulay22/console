import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Loader } from "./Loader";

export default {
  title: "Foundation/Loader",
  component: Loader,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {},
} as ComponentMeta<typeof Loader>;

export const Defaults: ComponentStory<typeof Loader> = () => <Loader />;

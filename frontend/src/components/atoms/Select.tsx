import styled from "styled-components";

export const Select = styled.select`
  height: 35px;
  font-size: 16px;
  padding-left: var(--spacing-lg);
  background: var(--color-nu-10);
  border: 1px solid var(--color-nu-90);
  border-radius: var(--radius-sm);
  color: var(--color-nu-90);
`;

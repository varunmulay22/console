export const enum Size {
  Small = "Size.Small",
  Medium = "Size.Medium",
  Large = "Size.Large",
}

import React from "react";
import { Text } from "../typography/text";
import { Size } from "./Size";
import { Intent } from "./Intent";
import styled from "styled-components";

interface ButtonProps {
  /**
   * What size of button
   */
  size?: Size;
  /**
   * What is the intent is the button communicating?
   */
  intent?: Intent;
  /**
   * Displays the button in a minimal style
   */
  secondary?: boolean;
  /**
   * Additional styling to pass to the button component
   */
  style?: React.CSSProperties;
  /**
   * Can the user interact with the button?
   */
  disabled?: boolean;
  /**
   * Callback function when the button is clicked
   */
  onClick?: React.EventHandler<React.MouseEvent>;
  type?: "button" | "submit";
  label?: string;
  icon?: React.ReactNode;
}

export const StyledButton = styled.button<ButtonProps>`
  border: none;
  background: ${({ intent = Intent.None }) =>
    ({
      [Intent.None]: "var(--gradient-info)",
      [Intent.Danger]: "var(--gradient-danger)",
      [Intent.Warning]: "var(--gradient-warning)",
      [Intent.Info]: "var(--gradient-info)",
      [Intent.Success]: "var(--gradient-success)",
    }[intent])};
  color: var(--color-nu-100);
  position: relative;
  z-index: 0;
  min-height: ${({ size = Size.Medium }) =>
    ({
      [Size.Small]: "16px",
      [Size.Medium]: "35px",
      [Size.Large]: "calc(2 * var(--spacing-md) + 16px)",
    }[size])};
  padding: ${({ size = Size.Medium }) =>
    ({
      [Size.Small]: "3px",
      [Size.Medium]: "8px 15px",
      [Size.Large]: "calc(2 * var(--spacing-md) + 16px)",
    }[size])};

  border-radius: var(--spacing-xxl);

  &:hover {
    background: ${({ intent = Intent.None }) =>
      ({
        [Intent.None]: "var(--color-gradient-40)",
        [Intent.Danger]: "var(--color-gradient-20)",
        [Intent.Warning]: "var(--color-gradient-90)",
        [Intent.Info]: "var(--color-gradient-40)",
        [Intent.Success]: "var(--color-gradient-70)",
      }[intent])};
  }
  &:disabled {
    pointer-events: none;
    opacity: 0.5;
  }

  ${(props) =>
    props.secondary &&
    `
      &::before,
      &::after {
        content: "";
        display: block;
        position: absolute;
        z-index: -1;
        
      }
      &::before {
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        border-radius: var(--radius-xl);
        background: ${
          {
            [Intent.None]: "var(--color-gradient-50)",
            [Intent.Danger]: "var(--gradient-danger)",
            [Intent.Warning]: "var(--gradient-warning)",
            [Intent.Info]: "var(--gradient-info)",
            [Intent.Success]: "var(--gradient-success)",
          }[props.intent || Intent.None]
        }
      }
      &::after {
        top: 2px;
        left: 2px;
        right: 2px;
        bottom: 2px;
        border-radius: calc(var(--radius-xl) - 2px);
        background: var(--color-nu-20);
      }
      &:hover::before {
        background: ${
          {
            [Intent.None]: "var(--color-gradient-40)",
            [Intent.Danger]: "var(--color-gradient-20)",
            [Intent.Warning]: "var(--color-gradient-90)",
            [Intent.Info]: "var(--color-gradient-40)",
            [Intent.Success]: "var(--color-gradient-70)",
          }[props.intent || Intent.None]
        };
      }
      
  `}
`;

export const Button: React.FC<ButtonProps> = ({
  style,
  label,
  onClick,
  size = Size.Medium,
  intent = Intent.None,
  icon,
  ...props
}) => (
  <StyledButton size={size} intent={intent} onClick={onClick} {...props}>
    {label && <Text size={{ [Size.Small]: 8, [Size.Medium]: 10, [Size.Large]: 14 }[size]}>{label}</Text>}
    {icon}
  </StyledButton>
);

import styled from "styled-components";
import { Card } from "./atoms/Card";
import { Tasks } from "./tasks/Tasks";

export const Sidebar = styled(Card)`
  padding: var(--spacing-sm) var(--spacing-md);
  width: 280px;

  display: flex;
  flex-direction: column;
  flex-shrink: 0;
  flex-grow: 1;
  gap: var(--spacing-lg);
`;

export const MockSidebar = () => (
  <Sidebar>
    <section>
      <Tasks />
    </section>
  </Sidebar>
);

import { useCallback } from "react";
import { useNavigate } from "react-router";
import styled from "styled-components";
import moment from "moment";
import blankProjectLogo from "../images/blank_project.svg";

import { Text } from "../components/typography/text";
import { Project } from "../model/project";

interface ProjectCardProps {
  id: number;
  name: string;
  phase?: string;
  description?: string;
  project: Project;
  previewUrl?: string;
}

const Card = styled.div`
  height: 280px;
  color: var(--color-nu-90);
  background: var(--color-brand-coolgray);
  border-radius: var(--radius-lg);
  boxshadow: 0px 1px 2px -1px rgba(0, 0, 0, 0.2), 0px 1px 10px -5px rgba(0, 0, 0, 0.5);
  overflow: clip;
  cursor: pointer;
  display: flex;
  flex-direction: column;
`;

export const ProjectCard: React.FC<ProjectCardProps> = ({ id, name, phase, description, project, previewUrl }) => {
  const navigateTo = useNavigate();
  const onClick = useCallback(() => navigateTo(`/projects/${id}`), []);

  return (
    <Card onClick={onClick}>
      <div
        style={{
          width: "100%",
          height: "175px",
          backgroundImage: `url(${previewUrl || blankProjectLogo})`,
          backgroundSize: "95% 90%",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center center",
        }}
      />
      <div
        style={{
          padding: "var(--spacing-lg) var(--spacing-md)",
          flexGrow: 1,
          display: "flex",
          gap: "var(--spacing-md)",
          flexDirection: "column",
          justifyContent: "space-between",
        }}
      >
        <Text size={13}>{name}</Text>
        <Text size={10} style={{ color: "var(--color-nu-60)", flexGrow: 1 }}>
          {description}
        </Text>
        <Text size={10} style={{ color: "var(--color-nu-50)", textTransform: "capitalize" }}>
          <>
            {phase?.replace("_", "-")} &bull; Go-live{" "}
            <span title={moment(project.goLiveAt).format("LLL")}>
              {project.goLiveAt ? moment(project.goLiveAt).fromNow() : "Not Set"}
            </span>{" "}
            &bull; updated {moment(project.updatedAt).fromNow()}
          </>
        </Text>
      </div>
    </Card>
  );
};

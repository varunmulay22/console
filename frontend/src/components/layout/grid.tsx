import styled from "styled-components";

interface IGridProps {
  itemWidth: string;
  itemMaxWidth: string;
  gap: string;
}

export const Grid = styled.div<IGridProps>`
  display: grid;
  gap: ${({ gap }) => gap};
  grid-template-columns: repeat(
    auto-fit,
    minmax(${({ itemWidth }) => itemWidth}, ${({ itemMaxWidth }) => itemMaxWidth})
  );
`;

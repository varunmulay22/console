import styled from "styled-components";

interface IColumnProps {
  gap: string;
}

export const Column = styled.div<IColumnProps>`
  display: flex;
  flex-direction: column;
  gap: ${({ gap }) => gap};
`;

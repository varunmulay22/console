import styled from "styled-components";

interface IRowProps {
  gap: string;
}

export const Row = styled.div<IRowProps>`
  display: flex;
  gap: ${({ gap }) => gap};
`;

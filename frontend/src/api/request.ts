import axios from "axios";

const baseConfig = {
  baseURL: process.env.REACT_APP_API_URL,
  origin: process.env.PUBLIC_URL,
  headers: {
    Accept: "application/json",
  },
};

export const request = axios.create(baseConfig);

request.interceptors.request.use((req) => {
  if (!req.headers) {
    req.headers = {};
  }
  const token = localStorage.getItem("synura-bearer-token");
  if (token && token !== "undefined") {
    req.headers.Authorization = token as string;
  }

  return req;
});

import { request } from "./request";

interface IAsset {
  id: number;
  name: string;
}

/**
 * Get a specific asset by ID
 * @param assetId number
 * @returns Promise<IAsset>
 */
export async function getAsset(assetId: number): Promise<IAsset> {
  const { data } = await request.get(`/assets/${assetId}`);
  return data;
}

export async function deleteAsset(assetId: number): Promise<void> {
  return request.delete(`/assets/${assetId}`);
}

export async function createAsset(name: string = "", description: string = ""): Promise<IAsset> {
  //TODO
  return { id: 0, name: "" };
}

export async function updateAsset(asset: IAsset): Promise<IAsset> {
  const { data } = await request.put(`/assets/${asset.id}`, asset);
  return data;
}

import camelcaseKeys from "camelcase-keys";
import snakecaseKeys from "snakecase-keys";
import { Task, NewTask } from "../model/task";
import { request } from "./request";

/**
 * Get a list of all tasks
 * @returns Task[]
 */
export async function fetchTasks(): Promise<Task[]> {
  const { data, status } = await request.get("/tasks");
  if (status === 200) {
    return camelcaseKeys(data, { deep: true });
  } else {
    throw new Error(`Error fetching tasks: ${status}`);
  }
}

/**
 * Updates a task item
 * @returns Task
 */
export async function updateTask(updates: Partial<Task>): Promise<Task[]> {
  const { data, status } = await request.put(`/tasks/${updates.id}`, { task: updates });
  if (status === 200) {
    return camelcaseKeys(data, { deep: true });
  } else {
    throw new Error(`Error updating task: ${status}`);
  }
}

/**
 * Creates a task item
 * @returns Task
 */
export async function createTask(task: NewTask): Promise<Task> {
  const newTask = snakecaseKeys(task);
  const { data, status } = await request.post("/tasks", newTask);
  if (status === 201) {
    return data;
  } else {
    throw new Error(`Error creating a task: ${status}`);
  }
}

export async function deleteTask(task: Partial<Task>): Promise<Task[]> {
  const { data, status } = await request.delete(`/tasks/${task.id}?project_id=${task.projectId}`);
  if (status === 204) {
    return camelcaseKeys(data, { deep: true });
  } else {
    throw new Error(`Error deleting task: ${status}`);
  }
}

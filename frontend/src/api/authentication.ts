import { AxiosError } from "axios";
import snakecaseKeys from "snakecase-keys";
import toast from "react-hot-toast";
import { request } from "./request";

const IS_SUCCESS_STATUS = (statusCode: number) => statusCode >= 200 && statusCode < 300;

export async function logIn(email: string, password: string) {
  try {
    const {
      status,
      data,
      headers: { authorization },
    } = await request.post("/users/sign_in", {
      user: {
        email,
        password,
      },
    });
    if (status === 200) {
      return { ...data, authorization };
    }
    throw new Error("Something went wrong");
  } catch (error: AxiosError | unknown) {
    if ((error as AxiosError).name === "AxiosError") {
      return (error as AxiosError)?.response?.data;
    }
    throw new Error("Some unknown error occurred");
  }
}

export async function resendConfirmation(email: string) {
  try {
    await request.post(`/users/confirmation?email=${email}`);
    toast.success("If your email is in our system, you will receive an email shortly with a new confirmation link.");
  } catch (error: AxiosError | unknown) {
    toast.error("Sorry, something went wrong");
    if ((error as AxiosError).name === "AxiosError") {
      return (error as AxiosError)?.response?.data;
    }
    throw new Error("Some unknown error occurred");
  }
}

export async function register(
  full_name: string,
  email: string,
  password: string,
  password_confirmation: string,
  tos_acceptance: boolean
) {
  try {
    const {
      status,
      data,
      headers: { authorization },
    } = await request.post("/users", {
      user: {
        full_name,
        email,
        password,
        password_confirmation,
        tos_acceptance,
      },
    });
    if (status === 200) {
      return { ...data, authorization };
    }

    throw new Error();
  } catch (error: AxiosError | unknown) {
    if ((error as AxiosError).name === "AxiosError") {
      return (error as AxiosError)?.response?.data;
    }

    throw new Error();
  }
}

/**
 * Request password reset
 * @returns {}
 */
export async function forgotPassword(form: { email: string }): Promise<{}> {
  const { data, status } = await request.post("/users/password", { user: form });
  if (IS_SUCCESS_STATUS(status)) {
    return data;
  } else {
    throw new Error(`Error request password reset: ${status}`);
  }
}

/**
 * Confirms user email
 * @returns {}
 */
export async function confirmUserEmail(token: string): Promise<{}> {
  const { data, status } = await request.get(`/users/confirmation?confirmation_token=${token}`);
  if (IS_SUCCESS_STATUS(status)) {
    return data;
  } else {
    throw new Error(`Error confirming user email: ${status}`);
  }
}

/**
 * Change password
 * @returns {}
 */
export async function newPassword(newCreds: { resetPasswordToken: string; password: string }): Promise<{}> {
  const newPassword = snakecaseKeys(newCreds);
  const { data, status } = await request.put("/users/password", { user: newPassword });
  if (IS_SUCCESS_STATUS(status)) {
    return data;
  } else {
    throw new Error(`Error changing password: ${status}`);
  }
}

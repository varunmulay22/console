import { IProjectJSON, Project } from "../model/project";
import { request } from "./request";
import snakecaseKeys from "snakecase-keys";

/**
 * Fetch a list of all projects
 * @returns Project[]
 */
export async function listProjects(): Promise<Project[]> {
  const { data }: { data: IProjectJSON[] } = await request.get("/projects");
  return data.map(Project.fromJSON);
}

/**
 * Get a specific project by ID
 * @param projectId number
 * @returns Promise<Project>
 */
export async function getProject(projectId: number): Promise<Project> {
  const { data } = await request.get(`/projects/${projectId}`);
  return Project.fromJSON(data);
}

export async function deleteProject(projectId: number): Promise<void> {
  return request.delete(`/projects/${projectId}`);
}

export async function createProject(name: string = "", description: string = ""): Promise<Project> {
  const { data } = await request.post("/projects", { name, description });
  return Project.fromJSON(data);
}

export async function updateProject(project: Project): Promise<Project> {
  const { data } = await request.put(`/projects/${project.id}`, snakecaseKeys(project.toJSON()));
  return Project.fromJSON(data);
}

export async function getAssets(projectId: number): Promise<any> {
  const { data } = await request.get(`/projects/${projectId}/assets`);
  return data;
}

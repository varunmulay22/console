import styled from "styled-components";
import { useNavigate } from "react-router";
import { createProject } from "../api/project";
import { ProjectForm } from "../components/ProjectForm";
import { H3 } from "../components/typography/h3";
import { useRequestInterceptors } from "../hooks/useRequestInterceptors";

const Div = styled.div`
  flex: 1;
  input {
    width: 100%;
  }
`;

type FormInputs = {
  name: string;
  description: string;
};

export const NewProjectPage: React.FC = ({}) => {
  const navigate = useNavigate();
  const { error: pageError } = useRequestInterceptors();
  const handleCreateProject = (data: FormInputs) => {
    createProject(data.name, data.description).then((newProject) => {
      navigate(`/projects/${newProject.id}`);
    });
  };

  if (pageError) {
    return null;
  }
  return (
    <Div>
      <H3 style={{ marginBottom: "2rem" }}>Add new project</H3>
      <ProjectForm buttonText="Create project" onSubmit={handleCreateProject} />
    </Div>
  );
};

export default NewProjectPage;

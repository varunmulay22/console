import { useState } from "react";
import { Link, useSearchParams } from "react-router-dom";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import styled from "styled-components";
import { StyledButton } from "../components/atoms/Button";
import { Input } from "../components/atoms/Input";
import { Intent } from "../components/atoms/Intent";
import { Label } from "../components/atoms/Label";
import { H1 } from "../components/typography/h1";
import { Text } from "../components/typography/text";
import LoginBgASvg from "./login/login-bg-a.svg";
import LoginBgBSvg from "./login/login-bg-b.svg";
import { newPassword } from "../api/authentication";

const Container = styled.main`
  background: var(--color-brand-coolgray);
  color: var(--color-nu-90);
  padding: var(--spacing-xxl);
  border-radius: var(--radius-xl);
  width: 380px;
  max-width: 100%;
  margin: var(--spacing-xl) auto;
  position: relative;
  z-index: 1;
  display: flex;
  flex-direction: column;
  gap: var(--spacing-lg);
`;

const Button = styled(StyledButton)`
  display: flex;
  align-items: center;
`;

const ErrorMessage = styled(Text)`
  color: var(--color-brand-primary-100);
  margin-top: var(--spacing-xs);
`;

type FormInputs = {
  newPassword: string;
  confirmPassword: string;
};

const schema = yup
  .object({
    newPassword: yup.string().required("Type your new password"),
    confirmPassword: yup.string().oneOf([yup.ref("newPassword"), null], "Passwords must match"),
  })
  .required();

export default function NewPassword() {
  const [searchParams] = useSearchParams();
  const resetPasswordToken = searchParams.get("reset_password_token");
  const [isDone, setIsDone] = useState(false);
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<FormInputs>({
    resolver: yupResolver(schema),
  });
  const onSubmit = (data: FormInputs) => {
    newPassword({
      resetPasswordToken: resetPasswordToken || "",
      password: data.newPassword,
    })
      .then(() => {
        setIsDone(true);
        reset();
      })
      .catch(() => {
        console.error("Something went wrong!");
      });
  };

  return (
    <>
      <LoginBgASvg />
      <LoginBgBSvg />

      {isDone ? (
        <div
          style={{
            background: "var(--color-nu-20)",
            maxWidth: "850px",
            margin: "auto",
            display: "flex",
            padding: "20px",
            gap: "30px",
            borderRadius: "var(--radius-lg)",
            borderColor: "var(--color-gradient-30)",
            borderWidth: "1px",
          }}
        >
          <div>
            <Text size={12}>
              Done! Your password has been changed. You can now <Link to="/">login</Link>.
            </Text>
          </div>
        </div>
      ) : (
        <Container>
          <H1 style={{ textAlign: "center" }}>New password</H1>
          <form
            style={{ display: "grid", marginBottom: "var(--spacing-sm)", gap: "var(--spacing-lg)" }}
            onSubmit={handleSubmit(onSubmit)}
          >
            <Label>
              <Text size={12}>New password</Text>
              <Input {...register("newPassword")} type="password" />
              {errors.newPassword && <ErrorMessage>{errors.newPassword?.message}</ErrorMessage>}
            </Label>
            <Label>
              <Text size={12}>Confirm password</Text>
              <Input {...register("confirmPassword")} type="password" />
              {errors.confirmPassword && <ErrorMessage>{errors.confirmPassword?.message}</ErrorMessage>}
            </Label>
            <div>
              <Button type="submit" intent={Intent.Info}>
                <Text>Submit</Text>
              </Button>
            </div>
          </form>
        </Container>
      )}
    </>
  );
}

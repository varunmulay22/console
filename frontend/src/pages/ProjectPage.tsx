import { Button, StyledButton } from "../components/atoms/Button";
import { MockSidebar } from "../components/sidebar";
import { Text } from "../components/typography/text";
import { useNavigate, useParams } from "react-router";
import { Project } from "../model/project";
import { usePromise } from "../utils/usePromise";
import { getProject, deleteProject, updateProject, getAssets } from "../api/project";
import { request } from "../api/request";
import React, { ChangeEvent, FormEvent, useCallback, useEffect, useState } from "react";
import { Input } from "../components/atoms/Input";
import { Select } from "../components/atoms/Select";
import { Label } from "../components/atoms/Label";
import { Intent } from "../components/atoms/Intent";
import { AssetCard } from "../components/asset-card";
import { TitleCard } from "../components/title-card";
import { BiPencil } from "react-icons/bi";
import { ActionBar } from "../components/actionbar";
import { Loader } from "../components/atoms/Loader";
import styled from "styled-components";
import moment from "moment";
import { Row } from "../components/layout/Row";
import { Column } from "../components/layout/Column";
import { Link } from "react-router-dom";
import { useRequestInterceptors } from "../hooks/useRequestInterceptors";

interface ProjectPageProps {}

const Table = styled.table`
  border-collapse: collapse;
  thead th {
    padding: var(--radius-md) var(--radius-lg);
    font-weight: 300;
  }
  td {
    vertical-align: top;
  }
  tbody {
    background: var(--color-nu-20);
    tr:first-child {
      td:first-child {
        border-top-left-radius: var(--radius-lg);
      }
      td:last-child {
        border-top-right-radius: var(--radius-lg);
      }
    }
    tr:last-child {
      td:first-child {
        border-bottom-left-radius: var(--radius-lg);
      }
      td:last-child {
        border-bottom-right-radius: var(--radius-lg);
      }
    }
    td {
      padding: var(--spacing-lg);
    }
  }
`;

const AssetRow = styled.tr`
  border-bottom: 1px solid var(--color-nu-30);
  &:hover {
    background: var(--color-nu-10);
  }
  &:last-child {
    border-bottom: none;
  }
`;

const ProjectPage: React.FC<ProjectPageProps> = () => {
  const params = useParams();
  const [name, setName] = useState("");
  const [phase, setPhase] = useState<string>();
  const [goLiveAt, setGoLiveAt] = useState<string>();
  const [description, setDescription] = useState<string>();
  const [isEditing, setIsEditing] = useState(false);
  const { error: pageError } = useRequestInterceptors();
  const getProjectFn = useCallback(async () => {
    return getProject(params.project as any as number);
  }, [params.project]);

  const [state, result] = usePromise(getProjectFn);

  const [project, setProject] = useState(new Project({ id: 0 }));

  const [assets, setAssets] = useState<any[]>([]);

  useEffect(() => {
    (async () => {
      const assetResponse = await getAssets(params.project as any as number);
      setAssets(assetResponse);
    })();
  }, [params.project]);

  const navigate = useNavigate();

  useEffect(() => {
    if (state === "resolved" && result !== undefined) {
      setProject(result);
      setName(result.name);
      setPhase(result.phase);
      setGoLiveAt(result.goLiveAt);
      setDescription(result.description);
      if (!result.name) {
        setIsEditing(true);
      }
    }
  }, [state]);

  const updateName = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
  }, []);

  const updatePhase = useCallback((event: ChangeEvent<HTMLSelectElement>) => {
    setPhase(event.currentTarget.value);
  }, []);

  const updateGoLiveAt = useCallback((event: ChangeEvent<HTMLInputElement>) => {
    setGoLiveAt(event.target.value);
  }, []);

  const updateDescription = useCallback((event: ChangeEvent<HTMLInputElement>) => {
    setDescription(event.target.value);
  }, []);

  const doDelete = useCallback(async () => {
    await deleteProject(project.id);
    navigate(-1);
  }, [project.id]);

  const doSave = useCallback(
    async (event: FormEvent) => {
      project.name = name;
      project.phase = phase;
      project.goLiveAt = moment(goLiveAt).local().toISOString() || "";
      project.description = description;

      event.preventDefault();

      const updatedProject = await updateProject(project);
      setProject(updatedProject);
      setIsEditing(false);
    },
    [name, phase, goLiveAt, description, project]
  );

  const onInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.files?.length) {
      uploadFile(event.target.files);
    }
  };

  const uploadFile = async (files: FileList) => {
    const data = new FormData();
    if (files !== null) {
      for (let i = 0; i < files.length; i++) {
        data.append("asset[asset_file]", files[i]);
        data.set("asset[name]", files[0].name);
      }
    }
    data.set("asset[project_id]", String(project.id));

    const uploadResponse = await request.post(`/upload`, data);
    setAssets((l) => [...l, uploadResponse.data]);
    console.log(uploadResponse);
  };

  if (pageError) {
    return null;
  }

  return (
    <>
      <div
        style={{
          flexGrow: 1,
        }}
      >
        {
          {
            pending: (
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  flexDirection: "column",
                  height: "80vh",
                }}
              >
                <Loader />
              </div>
            ),
            rejected: null,
            resolved: (
              <Column gap="var(--spacing-md)">
                <TitleCard
                  title={
                    <>
                      {project.name}
                      <BiPencil
                        data-testid="edit-project"
                        style={{
                          fontSize: 16,
                          marginLeft: "var(--spacing-md)",
                          color: "var(--color-nu-70)",
                          cursor: "pointer",
                        }}
                        onClick={() => {
                          setIsEditing(!isEditing);
                        }}
                      />
                    </>
                  }
                  subtitle={
                    <>
                      {project.phase?.replace("_", "-")}
                      <br />
                      Go-live {goLiveAt ? moment(goLiveAt.toString()).fromNow() : "Not Set"}{" "}
                      {goLiveAt ? "(" + moment(goLiveAt?.toString()).format("LLL") + ")" : ""}
                    </>
                  }
                  description={project.description}
                />
                {isEditing && (
                  <>
                    <Label>
                      <Text>Project name</Text>
                      <Input value={name} onChange={updateName} />
                    </Label>
                    <Label>
                      <Text>Go-live (local time)</Text>
                      <Input
                        type="datetime-local"
                        value={goLiveAt ? moment(goLiveAt).format("YYYY-MM-DDTHH:mm") : ""}
                        onChange={updateGoLiveAt}
                      />
                    </Label>
                    <Label>
                      <Text>Phase</Text>
                      <Select onChange={updatePhase} value={phase}>
                        <option key="draft" value="draft">
                          Draft
                        </option>
                        <option key="pre_production" value="pre_production">
                          Pre-production
                        </option>
                        <option key="production" value="production">
                          Production
                        </option>
                        <option key="post_production" value="post_production">
                          Post-production
                        </option>
                        <option key="publishing" value="publishing">
                          Publishing
                        </option>
                        <option key="retrospective" value="retrospective">
                          Retrospective
                        </option>
                        <option key="archived" value="archived">
                          Archived
                        </option>
                      </Select>
                    </Label>
                    <Label>
                      <Text>Description</Text>
                      <Input value={description || ""} onChange={updateDescription} />
                    </Label>
                    <Row style={{ marginTop: "1rem" }} gap="var(--spacing-md)">
                      <Button label="Save" intent={Intent.Info} onClick={doSave} />
                      <Button label="Cancel" secondary intent={Intent.Info} onClick={() => setIsEditing(false)} />
                      <Button
                        label="Delete project"
                        intent={Intent.Danger}
                        secondary
                        onClick={() => {
                          if (
                            window.confirm(
                              "Are you sure you want to delete this project and all files in it? This cannot be undone."
                            )
                          ) {
                            doDelete();
                          }
                        }}
                      />
                    </Row>
                  </>
                )}
                <div className="hide-large">{project.name != null && <MockSidebar />}</div>
                {!isEditing && (
                  <ActionBar
                    actions={
                      <>
                        <Link to="/">
                          <Button label="Return to index" secondary style={{ display: "flex", alignItems: "center" }} />
                        </Link>
                        <Label tabIndex={0}>
                          <Input type="file" accept="*" onChange={onInputChange} style={{ display: "none" }}></Input>
                          <StyledButton as="div" style={{ display: "flex", alignItems: "center" }}>
                            <Text>Add file</Text>
                          </StyledButton>
                        </Label>
                      </>
                    }
                  />
                )}
                {assets.length > 0 && !isEditing && (
                  <Table>
                    <thead style={{ color: "var(--color-nu-60)", textAlign: "left" }}>
                      <tr>
                        <th>
                          <Text size={10}>Preview</Text>
                        </th>
                        <th>
                          <Text size={10}>Name</Text>
                        </th>
                        <th>
                          <Text size={10}>Size</Text>
                        </th>
                        <th>
                          <Text size={10}>Activity</Text>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {assets.map((e: any) => (
                        <AssetRow onClick={() => navigate(`assets/${e.id}`)}>
                          <td>
                            <AssetCard {...e} />
                          </td>
                          <td>
                            <Text>{e.name}</Text>
                            <Text>{e.description}</Text>
                          </td>
                          <td>
                            <Text>{e.mbSize || "< 1"} MB</Text>
                          </td>
                          <td>
                            <Text>Created {moment(e.createdAt).fromNow()}</Text>
                          </td>
                        </AssetRow>
                      ))}
                    </tbody>
                  </Table>
                )}
                {assets.length === 0 && !isEditing && (
                  <Text
                    style={{
                      marginTop: "var(--spacing-lg)",
                    }}
                  >
                    No files have been uploaded to this project yet.
                  </Text>
                )}
              </Column>
            ),
          }[state]
        }
      </div>
      <div className="hide-small">{project.name != null && <MockSidebar />}</div>
    </>
  );
};

export default ProjectPage;

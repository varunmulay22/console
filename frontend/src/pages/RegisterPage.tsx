import React, { useCallback, useContext, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import styled from "styled-components";
import { StyledButton } from "../components/atoms/Button";
import { Input } from "../components/atoms/Input";
import { Intent } from "../components/atoms/Intent";
import { Label } from "../components/atoms/Label";
import { H1 } from "../components/typography/h1";
import { Text } from "../components/typography/text";
import { AuthContext } from "../utils/authentication";
import LoginBgASvg from "./login/login-bg-a.svg";
import LoginBgBSvg from "./login/login-bg-b.svg";

const LoginContainer = styled.main`
  background: var(--color-brand-coolgray);
  color: var(--color-nu-90);
  padding: var(--spacing-xxl);
  border-radius: var(--radius-xl);
  max-width: 300px;
  min-width: 380px;
  margin: var(--spacing-xl) auto;
  position: relative;
  z-index: 1;
  display: flex;
  flex-direction: column;
  gap: var(--spacing-lg);
`;

const StyledDiv = styled.div`
  display: flex;
  flex-direction: row;
  gap: var(--spacing-sm);
  align-items: center;
`;

const StyledLabel = styled(Label)`
  display: inline-block;
`;

export default function RegisterPage() {
  const navigate = useNavigate();

  const [fullName, setFullName] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [passwordConfirm, setPasswordConfirm] = useState<string>("");
  const [acceptTerms, setAcceptTerms] = useState(false);
  const [dirtyFullName, setDirtyFullName] = useState(false);
  const [dirtyEmail, setDirtyEmail] = useState(false);
  const [dirtyPass, setDirtyPass] = useState(false);

  const authContext = useContext(AuthContext);

  if (authContext.authenticated) {
    navigate("/");
  }

  const register = useCallback(
    async (event: React.FormEvent | React.MouseEvent) => {
      event.preventDefault();
      event.stopPropagation();

      authContext.register(fullName, email, password, passwordConfirm, acceptTerms);
    },
    [fullName, email, password, passwordConfirm, password, acceptTerms]
  );

  const emailParts = email.split("@");
  const emailDomain = emailParts.length > 1 ? emailParts[1].split(".") : [];
  const validEmail =
    emailParts.length === 2 &&
    emailDomain.length > 1 &&
    emailDomain.reduce<boolean>((acc, curr) => curr.length > 0 && acc, true);
  const validPassword = password.length > 0 && password === passwordConfirm;
  const canSubmit = validPassword && fullName.length > 0 && email.length > 0;

  return (
    <>
      <LoginBgASvg />
      <LoginBgBSvg />

      <LoginContainer>
        <H1 style={{ textAlign: "center" }}>Sign up</H1>
        {authContext.error && (
          <div
            style={{
              color: "var(--color-nu-90)",
              border: "1px solid var(--color-danger-100)",
              padding: "var(--spacing-sm)",
              marginBottom: "var(--spacing-md)",
              borderRadius: "var(--radius-sm)",
            }}
          >
            <Text size={8}>{authContext.error}</Text>
          </div>
        )}
        <form
          style={{ display: "grid", marginBottom: "var(--spacing-sm)", gap: "var(--spacing-lg)" }}
          onSubmit={register}
        >
          <Label>
            <Text size={12}>Full name</Text>
            <Input
              value={fullName}
              onChange={(event) => setFullName(event.target.value)}
              onBlur={() => setDirtyFullName(true)}
              type="text"
              required
            />
            {dirtyFullName && fullName.length === 0 && (
              <Text size={8} style={{ color: "var(--color-danger-100)" }}>
                Name required
              </Text>
            )}
          </Label>
          <Label>
            <Text size={12}>Email address</Text>
            <Input
              value={email}
              onChange={(event) => setEmail(event.target.value)}
              onBlur={() => setDirtyEmail(true)}
              type="email"
            />
            {dirtyEmail && email.length === 0 && (
              <Text size={8} style={{ color: "var(--color-danger-100)" }}>
                Email address required
              </Text>
            )}
            {dirtyEmail && email.length > 0 && !validEmail && (
              <Text size={8} style={{ color: "var(--color-danger-100)" }}>
                Email address is invalid
              </Text>
            )}
          </Label>
          <Label>
            <Text size={12}>Password</Text>
            <Input value={password} onChange={(event) => setPassword(event.target.value)} type="password" />
          </Label>
          <Label>
            <Text size={12}>Confirm password</Text>
            <Input
              value={passwordConfirm}
              onChange={(event) => setPasswordConfirm(event.target.value)}
              type="password"
              onBlur={(event) => setDirtyPass(true)}
            />
            {dirtyPass && !validPassword && (
              <Text size={8} style={{ color: "var(--color-danger-100)" }}>
                Passwords do not match
              </Text>
            )}
          </Label>
          <StyledDiv>
            <Input
              type="checkbox"
              name="acceptTerms"
              id="acceptTerms"
              defaultChecked={acceptTerms}
              onClick={() => setAcceptTerms(!acceptTerms)}
            />
            <StyledLabel htmlFor="acceptTerms">
              <Text size={8}>
                By creating a Synura account, I acknowledge the Synura{" "}
                <a href="https://www.synura.com/handbook/corporate/privacy-policy/" target="_blank">
                  privacy policy{" "}
                </a>
                and agree to the Synura{" "}
                <a href="https://www.synura.com/handbook/corporate/terms-of-use/" target="_blank">
                  terms of use.
                </a>
              </Text>
            </StyledLabel>
          </StyledDiv>
          <div style={{ justifyContent: "end", display: "flex", gap: "1rem" }}>
            <div>
              <StyledButton type="submit" disabled={!canSubmit} intent={Intent.Info} onClick={register}>
                <Text>Register</Text>
              </StyledButton>
            </div>
          </div>
        </form>
        <Text style={{ marginTop: "1rem" }} size={8}>
          Already have an account? <Link to="/">Sign In</Link>
        </Text>
      </LoginContainer>
    </>
  );
}

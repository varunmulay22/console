import { useEffect, useState } from "react";
import { Link, useSearchParams } from "react-router-dom";
import styled from "styled-components";
import { H1 } from "../components/typography/h1";
import { Text } from "../components/typography/text";
import LoginBgASvg from "./login/login-bg-a.svg";
import LoginBgBSvg from "./login/login-bg-b.svg";
import { confirmUserEmail } from "../api/authentication";

const Container = styled.main`
  background: var(--color-brand-coolgray);
  color: var(--color-nu-90);
  padding: var(--spacing-xxl);
  border-radius: var(--radius-xl);
  width: 380px;
  max-width: 100%;
  margin: var(--spacing-xl) auto;
  position: relative;
  z-index: 1;
  display: flex;
  flex-direction: column;
  gap: var(--spacing-lg);
`;

export default function ConfirmEmail() {
  const [searchParams] = useSearchParams();
  const confirmationToken = searchParams.get("confirmation_token");
  const [isDone, setIsDone] = useState(false);

  const confirmEmail = (token: string) => {
    confirmUserEmail(token)
      .then(() => {
        setIsDone(true);
      })
      .catch(() => {
        console.error("Something went wrong!");
      });
  };

  useEffect(() => {
    if (confirmationToken) {
      confirmEmail(confirmationToken);
    }
  }, [confirmationToken]);

  return (
    <>
      <LoginBgASvg />
      <LoginBgBSvg />

      {isDone ? (
        <div
          style={{
            background: "var(--color-nu-20)",
            maxWidth: "850px",
            margin: "auto",
            display: "flex",
            padding: "20px",
            gap: "30px",
            borderRadius: "var(--radius-lg)",
            borderColor: "var(--color-gradient-30)",
            borderWidth: "1px",
          }}
        >
          <div>
            <Text size={12}>
              Done! Your email is confirmed. You can now <Link to="/">login</Link>.
            </Text>
          </div>
        </div>
      ) : (
        <Container>
          <H1 style={{ textAlign: "center" }}>Please wait...</H1>
        </Container>
      )}
    </>
  );
}

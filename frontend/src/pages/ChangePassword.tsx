import { useState } from "react";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import styled from "styled-components";
import { Button } from "../components/atoms/Button";
import { Input } from "../components/atoms/Input";
import { Intent } from "../components/atoms/Intent";
import { Label } from "../components/atoms/Label";
import { H1 } from "../components/typography/h1";
import { Text } from "../components/typography/text";
import LoginBgASvg from "./login/login-bg-a.svg";
import LoginBgBSvg from "./login/login-bg-b.svg";
import { Link } from "react-router-dom";
import { forgotPassword } from "../api/authentication";
import { Row } from "../components/layout/Row";

const Container = styled.main`
  background: var(--color-brand-coolgray);
  color: var(--color-nu-90);
  padding: var(--spacing-xxl);
  border-radius: var(--radius-xl);
  width: 380px;
  max-width: 100%;
  margin: var(--spacing-xl) auto;
  position: relative;
  z-index: 1;
  display: flex;
  flex-direction: column;
  gap: var(--spacing-lg);
`;

const FlexButton = styled(Button)`
  display: flex;
  align-items: center;
`;

const ErrorMessage = styled(Text)`
  color: var(--color-brand-primary-100);
  margin-top: var(--spacing-xs);
`;

type FormInputs = {
  email: string;
};

const schema = yup
  .object({
    email: yup.string().email().required("Please provide a valid email address"),
  })
  .required();

export default function ForgotPassword() {
  const [isDone, setIsDone] = useState(false);
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<FormInputs>({
    resolver: yupResolver(schema),
  });
  const onSubmit = (data: FormInputs) => {
    forgotPassword(data)
      .then(() => {
        setIsDone(true);
        reset();
      })
      .catch(() => {
        console.error("Sorry, Something went wrong.");
      });
  };

  return (
    <>
      <LoginBgASvg />
      <LoginBgBSvg />

      {isDone ? (
        <div
          style={{
            background: "var(--color-nu-20)",
            maxWidth: "850px",
            margin: "auto",
            display: "flex",
            padding: "20px",
            gap: "30px",
            borderRadius: "var(--radius-lg)",
            borderColor: "var(--color-gradient-30)",
            borderWidth: "1px",
          }}
        >
          <div>
            <Text style={{ textAlign: "center" }} size={12}>
              If your email is in our system you will receive an email with instructions on how to reset your password
              in a few minutes. In the meantime you can <Link to="/">return home.</Link>
            </Text>
          </div>
        </div>
      ) : (
        <Container>
          <H1 style={{ textAlign: "center" }}>Change password</H1>
          <div>
            <Text style={{ marginBottom: "1rem" }} size={8}>
              Enter your email address below and you'll receive an email with a link to change your password.
            </Text>
          </div>
          <form
            style={{ display: "grid", marginBottom: "var(--spacing-sm)", gap: "var(--spacing-xl)" }}
            onSubmit={handleSubmit(onSubmit)}
          >
            <Label>
              <Text size={12}>Email address</Text>
              <Input {...register("email")} type="email" />
              {errors.email && <ErrorMessage>{errors.email?.message}</ErrorMessage>}
            </Label>
            <Row style={{ justifyContent: "end" }} gap="1rem">
              <div>
                <Link to="/">
                  <FlexButton label="Cancel" secondary intent={Intent.Info} />
                </Link>
              </div>
              <div>
                <FlexButton label="Submit" type="submit" intent={Intent.Info} />
              </div>
            </Row>
          </form>
        </Container>
      )}
    </>
  );
}

import React, { useCallback, useContext, useState } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { animated } from "@react-spring/web";
import { StyledButton } from "../components/atoms/Button";
import { Input } from "../components/atoms/Input";
import { Intent } from "../components/atoms/Intent";
import { Label } from "../components/atoms/Label";

import { H1 } from "../components/typography/h1";
import { Text } from "../components/typography/text";
import { AuthContext } from "../utils/authentication";
import { useErrorAnimation } from "../hooks/useErrorAnimation";

import LoginBgASvg from "./login/login-bg-a.svg";
import LoginBgBSvg from "./login/login-bg-b.svg";
import { validateEmail } from "../utils/validateEmail";

const LoginContainer = styled.main`
  background: var(--color-brand-coolgray);
  color: var(--color-nu-90);
  padding: var(--spacing-xxl);
  border-radius: var(--radius-xl);
  max-width: 380px;
  min-width: 380px;
  margin: var(--spacing-xl) auto;
  position: relative;
  z-index: 1;
  display: flex;
  flex-direction: column;
  gap: var(--spacing-lg);
`;

export default function LoginPage() {
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [emailError, setEmailError] = useState<string>("");
  const { x, animate, state } = useErrorAnimation();
  const authContext = useContext(AuthContext);

  const resendConfirmation = useCallback(
    async (event: React.FormEvent | React.MouseEvent) => {
      event.preventDefault();
      event.stopPropagation();
      const isValidEmail = validateEmail(email);
      if (isValidEmail) {
        authContext.resendConfirmation(email);
        setEmailError("");
      } else {
        animate(!state);
        setEmailError("Please provide a valid email address.");
      }
    },
    [email, state]
  );

  const logIn = useCallback(
    async (event: React.FormEvent | React.MouseEvent) => {
      event.preventDefault();
      event.stopPropagation();

      authContext.logIn(email, password, () => animate(!state));
    },
    [email, password, state]
  );

  return (
    <>
      <LoginBgASvg />
      <LoginBgBSvg />

      <LoginContainer>
        <H1 style={{ textAlign: "center" }}>Sign in</H1>
        {authContext.error && (
          <animated.div
            style={{
              translate: x.to({
                range: [0, 0.25, 0.35, 0.45, 0.55, 0.65, 0.75, 1],
                output: [-2, 2, -2, 2, -2, 2, -2, 0],
              }),
            }}
          >
            <div
              style={{
                color: "var(--color-nu-90)",
                border: "1px solid var(--color-danger-100)",
                padding: "var(--spacing-sm)",
                marginBottom: "var(--spacing-md)",
                borderRadius: "var(--radius-sm)",
              }}
            >
              <Text size={8}>
                {authContext.error !== "Not Found" && authContext.error}
                {authContext.error === "Not Found" && (
                  <>
                    You have entered an invalid email and password combination. Try entering them again, click{" "}
                    <Link to="/register">here</Link> to register a new account, or click{" "}
                    <Link to="/forgot-password">here</Link> to reset your password.
                  </>
                )}
                {authContext.error === "You have to confirm your email address before continuing." && (
                  <>
                    {" "}
                    Click{" "}
                    <Link to="#" onClick={resendConfirmation}>
                      here
                    </Link>{" "}
                    to send a new confirmation email.
                  </>
                )}
              </Text>
            </div>
          </animated.div>
        )}

        {emailError && (
          <animated.div
            style={{
              translate: x.to({
                range: [0, 0.25, 0.35, 0.45, 0.55, 0.65, 0.75, 1],
                output: [-2, 2, -2, 2, -2, 2, -2, 0],
              }),
            }}
          >
            <div
              style={{
                background: "var(--color-danger-100)",
                color: "var(--color-nu-90)",
                border: "1px solid var(--color-danger-100)",
                padding: "var(--spacing-sm)",
                marginBottom: "var(--spacing-md)",
                borderRadius: "var(--radius-sm)",
              }}
            >
              <Text size={8}>{emailError}</Text>
            </div>
          </animated.div>
        )}
        <form style={{ display: "grid", marginBottom: "var(--spacing-sm)", gap: "var(--spacing-lg)" }} onSubmit={logIn}>
          <Label>
            <Text size={12}>Email address</Text>
            <Input value={email} onChange={(event) => setEmail(event.target.value)} type="email" />
          </Label>
          <Label>
            <Text size={12}>Password</Text>
            <Input value={password} onChange={(event) => setPassword(event.target.value)} type="password" />
          </Label>
          <div style={{ justifyContent: "end", display: "flex", gap: "1rem" }}>
            <div>
              <StyledButton type="submit" intent={Intent.Info} onClick={logIn}>
                <Text>Log in</Text>
              </StyledButton>
            </div>
          </div>
        </form>
        <Text style={{ marginTop: "var(--spacing-xs)" }} size={8}>
          No account yet? <Link to="/register">Sign up</Link>
        </Text>
        <Text style={{ marginTop: "var(--spacing-xs)" }} size={8}>
          <Link to="/change-password">Forgot password?</Link>
        </Text>
        <Text style={{ marginTop: "var(--spacing-xs)" }} size={8}>
          Need a new confirmation email? Enter your email address above,{" "}
          <Link to="#" onClick={resendConfirmation}>
            click here
          </Link>
          , and then check your email.
        </Text>
      </LoginContainer>
    </>
  );
}

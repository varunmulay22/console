import { Button } from "../components/atoms/Button";
import { MockSidebar } from "../components/sidebar";
import { Text } from "../components/typography/text";
import { useNavigate, useParams } from "react-router";
import { usePromise } from "../utils/usePromise";
import React, { FormEvent, useCallback, useEffect, useState } from "react";
import { Input } from "../components/atoms/Input";
import { Label } from "../components/atoms/Label";
import { Intent } from "../components/atoms/Intent";
import { TitleCard } from "../components/title-card";
import { BiPencil } from "react-icons/bi";
import { Loader } from "../components/atoms/Loader";

import { Row } from "../components/layout/Row";
import { Column } from "../components/layout/Column";

import { request } from "../api/request";
import { Card } from "../components/atoms/Card";
import { AssetCard } from "../components/asset-card";
import moment from "moment";
import { ActionBar } from "../components/actionbar";
import { Link } from "react-router-dom";
import { useRequestInterceptors } from "../hooks/useRequestInterceptors";

interface ProjectPageProps {}

const AssetPage: React.FC<ProjectPageProps> = () => {
  const params = useParams();
  const navigate = useNavigate();
  const getAssetFn = useCallback(async () => {
    //TODO: update this to use api/asset getAsset once backend can infer projectId
    const { data } = await request(`/projects/${params.project}/assets/${params.asset}`);
    return data;
  }, [params.asset]);

  const [assetName, setAssetName] = useState("");
  const [assetDescription, setAssetDescription] = useState("");
  //TODO: proper types for asset response (json schema based?)
  const [asset, setAsset] = useState<any>();
  const { error: pageError } = useRequestInterceptors();
  const [state, assetResponse, error] = usePromise(getAssetFn);

  useEffect(() => {
    setAssetName(assetResponse?.name);
    setAssetDescription(assetResponse?.description);
    setAsset(assetResponse);
  }, [assetResponse]);

  const [isEditing, setIsEditing] = useState(false);

  const doSave = useCallback(
    async (event: FormEvent) => {
      event.preventDefault();
      //TODO: update this to use api/asset updateAsset once backend can infer projectId
      const { data } = await request.put(`/projects/${params.project}/assets/${params.asset}`, {
        ...asset,
        name: assetName,
        description: assetDescription,
      });
      setIsEditing(false);
      setAsset(data);
    },
    [asset, assetName, assetDescription]
  );
  const doDelete = async () => {
    //TODO: update this to use api/asset deleteAsset once backend can infer projectId
    await request.delete(`/projects/${params.project}/assets/${params.asset}`);
    navigate(-1);
  };

  if (pageError) {
    return null;
  }
  return (
    <>
      <div
        style={{
          flexGrow: 1,
        }}
      >
        {
          {
            pending: (
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  flexDirection: "column",
                  height: "80vh",
                }}
              >
                <Loader />
              </div>
            ),
            rejected: <Text>Error: {error?.code}</Text>,
            resolved: (
              <Column gap="var(--spacing-md)">
                <TitleCard
                  title={
                    <>
                      {asset?.name}
                      <BiPencil
                        data-testid="edit-asset"
                        style={{
                          fontSize: 16,
                          marginLeft: "var(--spacing-md)",
                          color: "var(--color-nu-70)",
                          cursor: "pointer",
                        }}
                        onClick={() => {
                          setIsEditing(!isEditing);
                        }}
                      />
                    </>
                  }
                  subtitle={<>Last updated: {moment(asset?.updatedAt).fromNow()}</>}
                  description={asset?.description}
                />
                {isEditing && (
                  <>
                    <Label>
                      <Text>Asset name</Text>
                      <Input
                        value={assetName}
                        onChange={(e) => {
                          setAssetName(e.target.value);
                        }}
                      />
                    </Label>
                    <Label>
                      <Text>Description</Text>
                      <Input
                        value={assetDescription}
                        onChange={(e) => {
                          setAssetDescription(e.target.value);
                        }}
                      />
                    </Label>

                    <Row style={{ marginTop: "1rem" }} gap="var(--spacing-md)">
                      <Button label="Save" intent={Intent.Info} onClick={doSave} />
                      <Button label="Cancel" secondary intent={Intent.Info} onClick={() => setIsEditing(false)} />
                      <Button
                        label="Delete file"
                        intent={Intent.Danger}
                        secondary
                        onClick={() => {
                          if (window.confirm("Are you sure you want to delete this asset? This cannot be undone.")) {
                            doDelete();
                          }
                        }}
                      />
                    </Row>
                  </>
                )}
                <ActionBar
                  actions={
                    <>
                      <Link to={`/projects/${params.project}`}>
                        <Button label="Return to project" secondary style={{ display: "flex", alignItems: "center" }} />
                      </Link>
                      {asset?.assetFileUrl && (
                        <a
                          target="_blank"
                          style={{ textDecoration: "underline", color: "var(--color-nu-100)" }}
                          href={asset?.assetFileUrl}
                        >
                          <Button label="Download file" />
                        </a>
                      )}
                    </>
                  }
                />
                <Card>
                  <AssetCard width="100%" height="480px" {...asset} />
                </Card>
                <div className="hide-large">{asset?.name != null && <MockSidebar />}</div>
              </Column>
            ),
          }[state]
        }
      </div>
      <div className="hide-small">{asset?.name != null && <MockSidebar />}</div>
    </>
  );
};

export default AssetPage;

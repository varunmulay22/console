export default function () {
  return (
    <svg
      style={{ position: "fixed", bottom: 0, left: 0, zIndex: -1 }}
      width="347"
      height="362"
      viewBox="0 0 434 452"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <circle opacity="0.15" cx="72" cy="362" r="362" fill="#6C34E8" />
      <circle cx="72" cy="362" r="286" fill="#25252D" />
      <circle opacity="0.15" cx="72" cy="362" r="219" fill="#6C34E8" />
    </svg>
  );
}

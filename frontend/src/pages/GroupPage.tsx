import { useCallback, useEffect, useState } from "react";

import { Button } from "../components/atoms/Button";
import { Grid } from "../components/layout/grid";
import { ProjectCard } from "../components/project-card";
import { Project } from "../model/project";
import { listProjects } from "../api/project";
import { useNavigate } from "react-router";
import { ActionBar } from "../components/actionbar";
import { useRequestInterceptors } from "../hooks/useRequestInterceptors";

const GroupPage: React.FC = () => {
  const navigate = useNavigate();
  const { error: pageError } = useRequestInterceptors();

  const [projects, setProjects] = useState<Project[]>([]);
  useEffect(() => {
    (async () => {
      const list = await listProjects();
      setProjects(list);
    })();
  }, []);

  const createProjectCallback = useCallback(async () => {
    navigate("/projects/new");
  }, []);

  if (pageError) {
    return null;
  }
  return (
    <>
      <div style={{ flexGrow: 1, display: "flex", flexDirection: "column", gap: "var(--spacing-md)" }}>
        <ActionBar actions={<Button label="Add project" onClick={createProjectCallback} />} />
        <Grid gap="var(--spacing-md)" itemMaxWidth="300px" itemWidth="256px">
          {projects.map((project) => (
            <ProjectCard
              key={project.id}
              id={project.id}
              name={project.name}
              description={project.description}
              phase={project.phase}
              project={project}
            />
          ))}
        </Grid>
      </div>
    </>
  );
};

export default GroupPage;

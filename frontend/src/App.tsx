import { AuthenticationProvider } from "./utils/authentication";
import { AppToast } from "./components/AppToast";
import { Router } from "./Router";

import "./styles.css";

export default function App() {
  return (
    <AuthenticationProvider>
      <AppToast />
      <Router />
    </AuthenticationProvider>
  );
}

import React, { Suspense, useContext } from "react";

import { BrowserRouter, Route, Routes } from "react-router-dom";
import { AuthContext } from "./utils/authentication";
import { Text } from "./components/typography/text";
import { Header } from "./components/header";
import ErrorBoundary from "./components/ErrorBoundary";

const GroupPage = React.lazy(() => import("./pages/GroupPage"));
const ProjectPage = React.lazy(() => import("./pages/ProjectPage"));
const AssetPage = React.lazy(() => import("./pages/AssetPage"));
const LoginPage = React.lazy(() => import("./pages/LoginPage"));
const ChangePassword = React.lazy(() => import("./pages/ChangePassword"));
const ConfirmEmail = React.lazy(() => import("./pages/ConfirmEmail"));
const NewPassword = React.lazy(() => import("./pages/NewPassword"));
const RegisterPage = React.lazy(() => import("./pages/RegisterPage"));
const CreateProjectPage = React.lazy(() => import("./pages/NewProjectPage"));

export function Router() {
  const { authenticated } = useContext(AuthContext);
  return (
    <BrowserRouter>
      <ErrorBoundary>
        <div className="Header">
          <Header title="Synura" />
        </div>
        <div className="App">
          <Suspense fallback={<Text>Loading</Text>}>
            <Routes>
              {authenticated === true ? (
                <>
                  <Route path="projects/new" element={<CreateProjectPage />}></Route>
                  <Route path="projects/:project" element={<ProjectPage />}></Route>
                  <Route path="projects/:project/assets/:asset" element={<AssetPage />}></Route>

                  <Route path="/users/confirmation" element={<ConfirmEmail />}></Route>

                  <Route path="" element={<GroupPage />} />
                </>
              ) : (
                <>
                  <Route path="/*" element={<LoginPage />}></Route>
                  <Route path="/users/confirmation" element={<ConfirmEmail />}></Route>

                  <Route path="register" element={<RegisterPage />}></Route>
                </>
              )}
              <Route path="register" element={<RegisterPage />}></Route>
              <Route path="/new-password" element={<NewPassword />}></Route>
              <Route path="/change-password" element={<ChangePassword />}></Route>
            </Routes>
          </Suspense>
        </div>
      </ErrorBoundary>
    </BrowserRouter>
  );
}

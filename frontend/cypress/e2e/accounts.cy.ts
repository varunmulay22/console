import { faker } from "@faker-js/faker";

let email = faker.internet.email();
let password = faker.internet.password();
let name = faker.name.fullName();

describe("Accounts", () => {
  it("Visits the home page", () => {
    cy.visit("http://localhost:9000");
  });

  it("Can click on sign up", () => {
    cy.visit("http://localhost:9000");
    cy.contains("Sign up").click();
    cy.contains("Sign up");
  });

  it("Shows an error when you try to register an account and the backend is down", () => {
    cy.visit("http://localhost:9000/register");
    cy.intercept("POST", "/users", (req) => {
      req.reply({
        statusCode: 500,
        body: { status: 500, error: "Internal Server Error" },
      });
    });
    cy.contains("Full name").type(name);
    cy.contains("Email address").type(email);
    cy.get("#acceptTerms").click({ force: true });
    cy.contains("Password").type(password);
    cy.contains("Confirm password").type(password);
    cy.contains("Register").click();
    cy.contains("Add project").should("not.exist");
    cy.contains("Internal Server Error");
  });

  it("Registers an account", () => {
    cy.clearLocalStorage();
    cy.clearCookies();
    cy.visit("http://localhost:9000/register");
    cy.contains("Full name").type(name);
    cy.contains("Email address").type(email);
    cy.get("#acceptTerms").click({ force: true });
    cy.contains("Password").type(password);
    cy.contains("Confirm password").type(password);
    cy.contains("Register").click();
    cy.contains("Add project");
    cy.contains("Something went wrong").should("not.exist");
    cy.contains("Internal Server Error").should("not.exist");
  });

  it("Disallows duplicate registration", () => {
    cy.visit("http://localhost:9000");
    cy.contains("Sign up").click();
    cy.contains("Full name").type(name);
    cy.contains("Email address").type(email);
    cy.contains("Password").type(password);
    cy.contains("Confirm password").type(password);
    cy.get("#acceptTerms").click();
    cy.contains("Register").click();
    cy.contains("Email has already been taken");
  });

  it("Shows an error when you try to sign in and the backend is down", () => {
    cy.visit("http://localhost:9000");
    cy.intercept("POST", "/users/sign_in", (req) => {
      req.reply({
        statusCode: 500,
        body: { status: 500, error: "Internal Server Error" },
      });
    });
    cy.contains("Email address").type(email);
    cy.contains("Password").type(password);
    cy.contains("Log in").click();
    cy.contains("Internal Server Error");
    cy.contains("Add project").should("not.exist");
  });

  it("Can sign in with new account", () => {
    cy.visit("http://localhost:9000");
    cy.contains("Email address").type(email);
    cy.contains("Password").type(password);
    cy.contains("Log in").click();
    cy.contains("Add project");
  });

  it("Can request password change when logged in", () => {
    // workaround due to lack of hover menu support
    cy.get(".dropdown").click();
    cy.contains("Change password").click();
    cy.contains("Email address").type(email);
    cy.contains("Submit").click();
    cy.contains(
      "If your email is in our system you will receive an email with instructions on how to reset your password"
    );
  });

  it("Shows an error if backend is down when you request a password change when logged in", () => {
    cy.visit("http://localhost:9000/change-password");
    cy.intercept("POST", "/users/password", (req) => {
      req.reply({
        statusCode: 500,
        body: { status: 500, error: "Internal Server Error" },
      });
    });
    cy.contains("Email address").type(email);
    cy.contains("Submit").click();
    cy.contains("Something went wrong");
  });

  it("Shows an error if backend is down when you request a password change", () => {
    cy.visit("http://localhost:9000/change-password");
    cy.intercept("POST", "/users/password", (req) => {
      req.reply({
        statusCode: 500,
        body: { status: 500, error: "Internal Server Error" },
      });
    });
    // workaround due to lack of hover menu support
    cy.contains("Email address").type(email);
    cy.contains("Submit").click();
    cy.contains("Something went wrong");
  });

  it("Can click on forgot password", () => {
    cy.visit("http://localhost:9000");
    cy.contains("Forgot password?").click();
    cy.contains("Email address");
  });

  it("Can request forgotten password", () => {
    cy.clearCookies();
    cy.clearLocalStorage();
    cy.visit("http://localhost:9000/change-password");
    cy.contains("Email address").type(email);
    cy.contains("Submit").click();
    cy.contains("If your email is in our system");
  });
});

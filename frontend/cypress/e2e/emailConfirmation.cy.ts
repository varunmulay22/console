import { faker } from "@faker-js/faker";

let email = faker.internet.email();

describe("Accounts", () => {
  it("Visits the home page", () => {
    cy.visit("http://localhost:9000");
  });

  it("Clicks to request email confirmation", () => {
    cy.contains("Sign in");
    cy.contains("click here").click();
  });

  it("Should get an error when requesting email confirmation without a valid email", () => {
    cy.contains("Please provide a valid email address.");
  });

  it("Should get a toast response with a valid email", () => {
    cy.contains("Email address").type(email);
    cy.contains("click here").click();
    cy.contains("If your email is in our system, you will receive an email shortly with a new confirmation link.");
  });
});

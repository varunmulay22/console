import { faker } from "@faker-js/faker";

let email = faker.internet.email();
let password = faker.internet.password();
let name = faker.name.fullName();

describe("Projects", () => {
  beforeEach(() => {
    cy.restoreLocalStorage();
  });

  afterEach(() => {
    cy.saveLocalStorage();
  });

  it("Registers an account", () => {
    cy.visit("http://localhost:9000/register");
    cy.contains("Full name").type(name);
    cy.contains("Email address").type(email);
    cy.get("#acceptTerms").click({ force: true });
    cy.contains("Password").type(password);
    cy.contains("Confirm password").type(password);
    cy.contains("Register").click();
    cy.contains("Add project");
    cy.contains("Something went wrong").should("not.exist");
    cy.contains("Internal Server Error").should("not.exist");
  });

  /*  it("Shows an error if it cannot open the demo project", () => {
    cy.intercept("GET", "/projects/**", (req) => {
      req.reply({
        statusCode: 500,
        body: { status: 500, error: "Internal Server Error" },
      });
    });
    cy.contains("My first project").click();
    cy.contains("My first project");
    cy.contains("Something went wrong");
    cy.get('[data-testid="edit-project"]').should("not.exist"); // Shouldn't render the project page it couldn't load
  });*/

  it("Can open the demo project", () => {
    cy.visit("http://localhost:9000/");
    cy.contains("My first project").click();
    cy.contains("My first project");
  });

  /*it("Shows an error if it cannot upload a file", () => {
    cy.intercept("POST", "/**", (req) => {
      req.reply({
        statusCode: 500,
        body: { status: 500, error: "Internal Server Error" },
      });
    });
    cy.get("input[type=file]").selectFile(
      { contents: "cypress/fixtures/file_example_MP4_640_3MG.mp4" },
      { force: true }
    );
    cy.contains("file_example_MP4_640_3MG.mp4").should("not.exist");
    cy.contains("Something went wrong");
  });*/

  it("Can add files", () => {
    cy.get("input[type=file]").selectFile(
      { contents: "cypress/fixtures/file_example_MP4_640_3MG.mp4" },
      { force: true }
    );
    cy.contains("file_example_MP4_640_3MG.mp4");
  });

  /*it("Cannot edit the demo project's name if the backend is down", () => {
    cy.intercept("PUT", "/**", (req) => {
      req.reply({
        statusCode: 500,
        body: { status: 500, error: "Internal Server Error" },
      });
    });
    cy.get('[data-testid="edit-project"]').click();
    cy.contains("Project name").type("{selectall}{backspace}").type("This is a new project name");
    cy.contains("Save").click();
    cy.contains("This is a new project name").should("not.exist");
    cy.contains("Something went wrong");
  });*/

  it("Can edit the demo project's name", () => {
    cy.get('[data-testid="edit-project"]').click();
    cy.contains("Project name").type("{selectall}{backspace}").type("This is a new project name");
    cy.contains("Save").click();
    cy.contains("This is a new project name");
  });

  it("Can edit the demo project's go-live", () => {
    cy.get('[data-testid="edit-project"]').click();
    cy.contains("Go-live Not Set");
    cy.contains("Go-live (local time)").type("2017-06-01T08:30");
    cy.contains("Save").click();
    cy.contains("Go-live Not Set").should("not.exist");
  });

  it("Can edit the demo project's phase", () => {
    cy.contains("draft");
    cy.get('[data-testid="edit-project"]').click();
    cy.contains("Phase").click().focused().select("Pre-production");
    cy.contains("Save").click();
    cy.contains("pre-production");
  });

  it("Can edit the demo project's description", () => {
    cy.get('[data-testid="edit-project"]').click();
    cy.contains("Description").type("{selectall}{backspace}").type("This is a new description");
    cy.contains("Save").click();
    cy.contains("This is a new description");
  });

  /*it("Cannot toggle tasks if the backend is down", () => {
    cy.intercept("PUT", "/**", (req) => {
      req.reply({
        statusCode: 500,
        body: { status: 500, error: "Internal Server Error" },
      });
    });
    cy.contains("My first task").click();
    cy.get("#task-item-0").should("be.checked");
    cy.contains("My first task").click();
    cy.get("#task-item-0").should("be.not.checked");
    cy.contains("Something is wrong");
  });*/

  it("Can toggle tasks", () => {
    cy.contains("My first task").click();
    cy.get("#task-item-0").should("be.checked");
    cy.contains("My first task").click();
    cy.get("#task-item-0").should("be.not.checked");
  });

  /*it("Cannot delete tasks if the backend is down", () => {
    cy.intercept("DELETE", "/**", (req) => {
      req.reply({
        statusCode: 500,
        body: { status: 500, error: "Internal Server Error" },
      });
    });
    cy.contains("Edit tasks").click();
    cy.contains("Delete").click();
    cy.contains("There are no tasks in this project").should("not.exist");
    cy.contains("My first task");
    cy.contains("Something is wrong");
  });*/

  it("Can rename tasks", () => {
    cy.contains("Edit tasks").click();
    cy.get('[data-testid="task-rename"]:visible').click().type("This is my new task{enter}");
    cy.contains("This is my new task");
  });

  it("Can delete tasks", () => {
    cy.get('[data-testid="task-delete"]').click();
    cy.contains("There are no tasks in this project");
  });

  it("Can finish editing tasks", () => {
    cy.contains("Done").click();
  });

  /*it("Cannot add tasks if the backend is down", () => {
    cy.intercept("POST", "/**", (req) => {
      req.reply({
        statusCode: 500,
        body: { status: 500, error: "Internal Server Error" },
      });
    });
    cy.contains("Edit tasks").click();
    cy.contains("New task description").click().type("This is a new task");
    cy.contains("Add").click();
    cy.contains("This is a new task").should("not.exist");
    cy.contains("Something is wrong");
  });*/

  it("Can add tasks", () => {
    cy.contains("Edit tasks").click();
    cy.get('[data-testid="task-input"]').click().type("This is a new task");
    cy.get('[data-testid="task-save"]').click();
    cy.contains("This is a new task");
  });

  /*it("Cannot delete the demo project if the backend is down", () => {
    cy.intercept("DELETE", "/**", (req) => {
      req.reply({
        statusCode: 500,
        body: { status: 500, error: "Internal Server Error" },
      });
    });
    cy.get('[data-testid="edit-project"]').click();
    cy.contains("Delete project").click();
    cy.contains("Something is wrong");
    cy.contains("This is a new project name");
  });*/

  it("Can delete the demo project", () => {
    cy.get('[data-testid="edit-project"]').click();
    cy.contains("Delete project").click();
    cy.contains("This is a new project name").should("not.exist");
  });

  /*it("Cannot add a project if the backend is down", () => {
    cy.visit("http://localhost:9000/projects");
    cy.intercept("POST", "/**", (req) => {
      req.reply({
        statusCode: 500,
        body: { status: 500, error: "Internal Server Error" },
      });
    });
    cy.contains("Add project").click();
    cy.contains("Name").click().type("My created project");
    cy.contains("Description").click().type("My created description");
    cy.contains("Create project").click();
    cy.contains("My created project");
    cy.contains("Something is wrong");
  });*/

  it("Can add a project", () => {
    cy.contains("Add project").click();
    cy.contains("Name").click().type("My created project");
    cy.contains("Description").click().type("My created description");
    cy.contains("Create project").click();
    cy.contains("My created project");
  });
});

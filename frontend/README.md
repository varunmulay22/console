# Frontend

test

This is our application front-end.

## Installing dependencies

1. Install [node & npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)
2. Run `npm install`

## Running the local environment

1. Run `npm start`

## Developing locally

You can also run the storybook instance locally to browse re-usable components and the design system

1. Run `npm run storybook`

## E2E integration and user tests

1. Run `npx cypress open` to start Cypress and run integration tests. If you prefer to not have tests re-run every time
   they are updated, you can use `npx cypress open --config watchForFileChanges=false` (note that this setting is
   persistent.)

If you prefer to run the tests from the commandline, you can use `npx cypress run`.

Cypress will run tests against your local environment, so you'll need both the backend and frontend running. We do not
currently have a Cypress cloud account, so there's no need to log in.

**Do not** run integration tests in staging or production as this will generate fake data in the environment.

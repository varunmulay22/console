<!-- Thanks for submitting a feature idea! If you're unsure about any section, you can leave it blank. We will eventually research and fill them out. The most important thing is the problem to solve. -->

# Problem to solve

<!-- What problem do we solve? Try to define the who/what/why of the opportunity as a user story. For example, "As a (who), I want (what), so I can (why/value)." -->

## Competitive advantage or differentiation

<!-- How does our version of this feature differentiate from what other products have? -->

## Intended users

<!-- Who will use this feature? Is the buyer the same as the intended user? -->

## Proposal

<!-- How are we going to solve the problem? Try to include the user journey!
For example, "The user should be able to use the UI/API to <perform a specific task>" -->

## Documentation

<!-- What documentation is required for this change? -->

## Permissions and Security

<!-- What permissions are required to perform the described actions? Are they consistent with the existing permissions as documented for users, groups, and projects as appropriate? Is the proposed behavior consistent between the UI, API, and other access methods (e.g. email replies)? -->

## Availability and Quality

<!-- What risks does this change pose to our availability? How might it affect the quality of the product? What additional test coverage or changes to tests will be needed? Will it require cross-browser testing? -->

## What does success look like, and how can we measure that?

<!-- Define both the success metrics and acceptance criteria. Note that success metrics indicate the desired business outcomes, while acceptance criteria indicate when the solution is working correctly. If there is no way to measure success, link to an issue that will implement a way to measure this. -->

## Pricing

<!-- Is this a basic feature, or is it associated with add-on usage pricing? -->

## Links/references

<!-- Add links to any other important references for this feature -->

/label ~"Type::Feature"

<!-- Customize/remove/add labels below as necessary -->

/label ~"Needs-Backend" ~"Needs-Frontend" ~"Needs-Video Processing"
/label ~Category::

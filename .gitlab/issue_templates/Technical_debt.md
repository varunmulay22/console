# Summary

<!-- Summarize the technical debt and the risk it brings. -->

## Original justification

<!-- Describe why this technical debt was taken on originally, including any trade-offs made and context for the decision. -->

## Relevant screenshots

<!-- Paste any relevant screenshots here, if any. -->

## Remediation needed

<!-- If you have an idea, describe how the it will eventually need to be remediated. -->

/label ~"Type::Technical debt"

<!-- Customize/remove/add labels below as necessary -->

/label ~"Needs-Backend" ~"Needs-Frontend" ~"Needs-Video Processing"
/label ~Category::

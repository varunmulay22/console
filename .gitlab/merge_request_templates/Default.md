# What does this MR do and why?

_Describe in detail what your merge request does and why._

<!--
Please keep this description updated with any discussion that takes place so
that reviewers can understand your intent. Keeping the description updated is
especially important if they didn't participate in the discussion.
-->

## Does this need a changelog entry?

<!--
Any Merge Request could receive an immediate changelog entry, it could be
[bundled in the next iteration review](https://www.synura.com/handbook/general/product_development/#iteration-reviews),
or maybe it isn't worth mentioning because it isn't user-facing.
-->

- [ ] Does not need a changelog entry (very small or not user-facing)
- [ ] Should be bundled in the next iteration (normal that isn't necessarily worth
      calling out immediately, but is fine in the next bundle)
- [ ] Should get an immediate changelog update (really exciting new feature)

## Screenshots or screen recordings

_These are strongly recommended to assist reviewers and reduce the time to merge your change._

<!--
Please include any relevant screenshots or screen recordings that will assist
reviewers and future readers. If you need help visually verifying the change,
please leave a comment and ping a maintainer.
-->

## How to set up and validate locally

_Numbered steps to set up and validate the change are strongly suggested._

<!--
Example below:

1. Enable the invite modal

   ```ruby
   Feature.enable(:invite_members_group_modal)
   ```

1. In the Rails console enable the experiment fully

   ```ruby
   Feature.enable(:member_areas_of_focus)
   ```

1. Visit any group or project member pages such as `http://127.0.0.1:3000/groups/flightjs/-/group_members`
1. Click the `invite members` button.

-->
